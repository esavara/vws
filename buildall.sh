#!/usr/bin/sh

# This script can only run on GNU/Linux You need Python and Go installed in
# your system. You also need to build Go in your system to cross compile for
# Windows, as read here:
# https://github.com/golang/go/wiki/WindowsCrossCompiling

# This script install and cross compile the Go part of the project to
# Windows. Makes the user manual, build the installer with Qt Installer
# Framework, etc.

bversion=2.0.0

# Get the absolute directory where this script is stored
ROOTDIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

cd "${ROOTDIR}"

# Check if the data directory exists for this repository
if [ ! -d installer/packages/com.deshackra.freelance.vws/data/ ]; then
    mkdir -p installer/packages/com.deshackra.freelance.vws/data/
else
    rm -rf installer/packages/com.deshackra.freelance.vws/data/
    mkdir -p installer/packages/com.deshackra.freelance.vws/data/
fi

# Copy all the Python files required for the project to work
cp main.py installer/packages/com.deshackra.freelance.vws/data/
cp -a src/ installer/packages/com.deshackra.freelance.vws/data/

# compile them to bytecode
python2 -m compileall -f -q -d com.deshackra.freelance.vws/data/ \
        installer/packages/com.deshackra.freelance.vws/data/

# and delete all Python files store there
find installer/packages/com.deshackra.freelance.vws/data/ -depth -mindepth 1 -name "*.py" -type f -delete

# copy the icon and the launcher!
cp -f res/app.ico installer/packages/com.deshackra.freelance.vws/data/
cp -f launch.bat installer/packages/com.deshackra.freelance.vws/data/

# make the user manual
## first delete any old artifact
rm doc/userguide.{aux,log,out,pdf,toc}
# make the manual
xelatex -output-directory=doc doc/userguide.tex
xelatex -output-directory=doc doc/userguide.tex
cp -f doc/userguide.pdf installer/packages/com.deshackra.freelance.vws/data/

# Done with Python.

# We cross-compile our Go code to run on Windows
# This assumes you have already cross-compiled the Go tools
# see: https://github.com/golang/go/wiki/WindowsCrossCompiling
# Everything was compiled with go version 1.4.2

GOOS=windows go get -f -u github.com/kardianos/osext
GOOS=windows go get -f -u github.com/codegangsta/negroni
GOOS=windows go get -f -u github.com/gorilla/mux
GOOS=windows go get -f -u gopkg.in/unrolled/render.v1
GOOS=windows go get -f -u github.com/darkhelmet/twitterstream
GOOS=windows go get -f -u bitbucket.org/tebeka/strftime

cd "${ROOTDIR}"

# Check if the data directory exists for this repository
if [ ! -d installer/packages/com.deshackra.freelance.asp/data/ ]; then
    mkdir -p installer/packages/com.deshackra.freelance.asp/data/
    mkdir -p installer/packages/com.deshackra.freelance.asp/data/Plugins
    mkdir -p installer/packages/com.deshackra.freelance.asp/data/Plugins/plugins
    mkdir -p installer/packages/com.deshackra.freelance.asp/data/Plugins/server
    mkdir -p installer/packages/com.deshackra.freelance.asp/data/Debug
    mkdir -p installer/packages/com.deshackra.freelance.asp/data/"Scrap output"
    mkdir -p installer/packages/com.deshackra.freelance.asp/data/Selections
else
    rm -rf installer/packages/com.deshackra.freelance.asp/data/
    mkdir -p installer/packages/com.deshackra.freelance.asp/data/
    mkdir -p installer/packages/com.deshackra.freelance.asp/data/Plugins
    mkdir -p installer/packages/com.deshackra.freelance.asp/data/Plugins/plugins
    mkdir -p installer/packages/com.deshackra.freelance.asp/data/Plugins/server
    mkdir -p installer/packages/com.deshackra.freelance.asp/data/Debug
    mkdir -p installer/packages/com.deshackra.freelance.asp/data/"Scrap output"
    mkdir -p installer/packages/com.deshackra.freelance.asp/data/Selections
fi

# Build all things Golang
GOOS=windows GOARCH=386 go build \
    -o installer/packages/com.deshackra.freelance.asp/data/Plugins/server/mainwebapp.exe \
    go/src/server/mainwebapp.go 

GOOS=windows GOARCH=386 go build \
    -o installer/packages/com.deshackra.freelance.asp/data/Plugins/plugins/twitter.exe \
    go/src/twitter-plugin/twitter.go

# Copy the templates for the server
cp -a go/src/server/templates/ \
   installer/packages/com.deshackra.freelance.asp/data/Plugins/server/

cp -a go/src/twitter-plugin/templates-twitter/ \
   installer/packages/com.deshackra.freelance.asp/data/Plugins/plugins/

cp -a go/src/bootstrap/ \
   installer/packages/com.deshackra.freelance.asp/data/Plugins/

if [ ! -d installer/packages/com.deshackra.freelance.python/data/ ]; then
    mkdir -p installer/packages/com.deshackra.freelance.python/data/
fi


# Download the x86 architecture Python interpreter for Windows
if [ ! -f installer/packages/com.deshackra.freelance.python/data/python.msi ]; then
    wget -O installer/packages/com.deshackra.freelance.python/data/python.msi \
         https://www.python.org/ftp/python/2.7.10/python-2.7.10.msi
fi
# Copy the requirements file too
cp -f requirements_windows.txt \
   installer/packages/com.deshackra.freelance.python/data/
# and a batch file that installs the Python interpreter
cp -f intr.bat installer/packages/com.deshackra.freelance.python/data/
# and another one for installing the dependencies
cp -f pip.bat installer/packages/com.deshackra.freelance.python/data/

# having copied all, mkdir the installer
cd "${ROOTDIR}"
wine binarycreator.exe -c installer/config/config.xml -p installer/packages \
     "installer - Visual Web Scraper pkgv$bversion"
