===============================
The core module `customwebview`
===============================

.. automodule:: src.customwebview
      :members:
      :undoc-members:
      :special-members:
      :exclude-members: __dict__,__weakref__,__module__
