================================================
Visual Web Scraper - source code's documentation
================================================

This documentation will help other developers to understand how the project was written and for what each thing is use for and how those things work together to bring this interesting peace of software into life.

If you are looking for the Golang documentation, please do this::

  $ ./startgodoc.sh

And follow the instructions output by the script.

.. toctree::
   :maxdepth: 2
   :caption: Table of Contents

   webframeinfo
   webelementinfo
   customwebview
   
