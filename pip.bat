REM This file installs the Python dependencies for Visual Web Scraper
REM install virtualenv
start /wait C:\Python27\Scripts\pip2.exe install virtualenv

REM create a virtualenv inside the target directory
start /wait C:\Python27\Scripts\virtualenv.exe %1

REM and finally, inside that virtualenv install the required dependencies
setlocal
call %1\Scripts\activate.bat
start /wait pip install -r "%userprofile%\requirements_windows.txt"
call deactivate.bat
endlocal
echo Returncode: %ERRORLEVEL%
