# this file is not part of the project Visual web scraper
#
# simply run all the tests to check the health of the project

if [ -z ${DISPLAY+x} ];
then
    export DISPLAY=:99;
    echo "DISPLAY set to ${DISPLAY}";
fi

nosetests -vv --with-coverage --cover-package=src
