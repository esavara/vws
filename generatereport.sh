# This file is not part of Visual Web Scraper
#
# Automagically generates a report with the date and the commit messages

echo "Project report #$(date --utc +%Y%m-%d)"
echo '[The intentions of this message is merely informative, but if there is any question'
echo 'or doubt in regard to the project, you can always ask me any question!]'
echo ""
echo "Hello, $1"'!'
echo ""
echo "Here is the report for the changes made to the project and each respective stats."
echo "The first change made is the first change listed here. Here they are:"
git log -n $2 --format="%n* Message: '%s'. Commit pushed by: '%ce'. Stats:" --shortstat --reverse
echo ""
echo 'Best regards!'
