#!/usr/bin/sh

# This script starts the Godoc documentation pointing to the Golang side of
# this project.

# Get the absolute directory where this script is stored
ROOTDIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
port=6060

echo "Visit in your web browser http://localhost:${port}/pkg/server/"
GOPATH=$ROOTDIR/go/ godoc -http=:${port}
