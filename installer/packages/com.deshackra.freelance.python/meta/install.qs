function Component() {}

Component.prototype.createOperationsForArchive = function(archive) {
    // add an extract operation with a modified path
    component.addOperation("Extract", archive, "@HomeDir@");
};

Component.prototype.createOperations = function() {
    component.createOperations();
    var pythonpath;

    // Is Python installed?
    wow6432 = installer.execute("reg", new Array("query",
        "HKLM\\Software\\Wow6432Node\\Python\\PythonCore\\2.7\\InstallPath"));
    arch32 = installer.execute("reg", new Array("query",
        "HKLM\\Software\\Python\\PythonCore\\2.7\\InstallPath"));
    if (wow6432[1] !== 0 && arch32[1] !== 0) {
        component.addElevatedOperation("Execute", "@HomeDir@/intr.bat");
    }

    // Install requirements
    component.addElevatedOperation("Execute", "@HomeDir@/pip.bat", "@TargetDir@");
    // Create shorcut
    component.addElevatedOperation("CreateShortcut", "@TargetDir@\\launch.bat",
        "@StartMenuDir@/Visual Web Scraper.lnk",
        "workingDirectory=@TargetDir@",
        "iconPath=@TargetDir@\\app.ico");
    component.addElevatedOperation("CreateShortcut", "@TargetDir@\\userguide.pdf",
        "@StartMenuDir@/User Guide - Visual Web Scraper.lnk",
        "workingDirectory=@TargetDir@");
    component.addElevatedOperation("CreateShortcut", "@TargetDir@\\launch.bat",
        "@HomeDir@/Desktop/Visual Web Scraper.lnk",
        "workingDirectory=@TargetDir@",
        "iconPath=@TargetDir@\\app.ico");
    // clean this mess
    component.addOperation("Delete", "@HomeDir@/python.msi");
    component.addOperation("Delete", "@HomeDir@/requirements_windows.txt");
    component.addOperation("Delete", "@HomeDir@/intr.bat");
    component.addOperation("Delete", "@HomeDir@/pip.bat");
};