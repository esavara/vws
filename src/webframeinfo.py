#!/usr/bin/env python2
# coding: utf-8
#                        VENI, SANCTE SPIRITUS

from PySide import QtCore
from . import webelementinfo
import logging
from hashlib import sha256

logger = logging.getLogger(__name__)


class WebFrames(QtCore.QObject):

    """ Holds information about children `QWebFrames` and makes easy
    navigating all children's iframe HTML tags.
    """

    def __init__(self, parent=None):
        """ Initialize the class
        """
        super(WebFrames, self).__init__(parent=parent)
        self.frameboxes = {}

    @property
    def childframes(self):
        """Returns all iframes with their names as sha256 strings
        :rtype: dict
        """
        frames = {}
        # get children frames
        children = self.parent().mainframe.childFrames()
        for frame in children:
            if not (frame.geometry().width() == 0 and
                    frame.geometry().height() == 0):
                name = sha256(frame.frameName()).hexdigest()
                frames[name] = frame
        return frames

    def maketagsinventory(self):
        """Make an inventory of each frame's HTML tags
        :return: None
        :rtype: None
        """
        self.frameboxes.clear()
        for name in self.childframes.iterkeys():
            boxes = self.childframes[name].findAllElements("*").toList()
            # remove invalid boxes or boxes without size
            for box in boxes:
                if (box.geometry().width() == 0 and
                        box.geometry().height() == 0)\
                        or box.isNull():
                    boxes.remove(box)
            # sort all the boxes by area
            boxes = sorted(boxes, key=lambda box: box.geometry().width() *
                           box.geometry().height(), reverse=False)
            self.frameboxes[name] = boxes

    def getelementbypos(self, pos, selecttype):
        """
        :param pos: a `QPoint` object
        :param selecttype: The element type for this element
        :return: Return the element under cursor pointer.
        :rtype: WebElement
        """
        child = None
        framename = None
        # inside which iframe/frame is the cursor posed?
        for name in self.childframes.iterkeys():
            if self.childframes[name].geometry().contains(pos.x(), pos.y()):
                logger.debug("Pointer inside iframe {}".format(name))
                framename = name
                break

        # Look and find the HTML tag which contains the current cursor position
        if framename:
            frame = self.childframes[framename]
            posrelativetoframe = pos - frame.pos()
            for box in self.frameboxes[framename]:
                if box.geometry().contains(posrelativetoframe.x(),
                                           posrelativetoframe.y()):
                    logger.debug("Tag element found")
                    child = box
                    break

        if child:
            # Return back an WebElement instance
            return webelementinfo.WebElement(child, selecttype,
                                             parent=self.parent())

    def getframebyname(self, name):
        """
        :param name: The name of the iframe, as sha256 string
        :return: the `QWebFrame` by its name
        :rtype: QWebFrame
        """
        if name in self.childframes:
            return self.childframes[name]
