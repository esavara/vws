from PySide import QtGui
from PySide.QtCore import QCoreApplication


def setup_package():
    if QtGui.qApp is None:
        QtGui.QApplication([])


def teardown_package():
    QCoreApplication.instance().quit()
