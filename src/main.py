#!/usr/bin/env python2
# coding: utf-8
#                        VENI, SANCTE SPIRITUS

from PySide import QtGui
from PySide import QtCore
from ui import main_ui as mainui
from . import allinonewidget
from itertools import cycle


class Ui(QtGui.QMainWindow, mainui.Ui_MainWindow):

    def __init__(self, parent=None):
        super(Ui, self).__init__(parent)
        self.setupUi(self)
        self.move(QtGui.QApplication.desktop().screen()
                  .rect().center() - self.rect().center())
        self.tbtCorner = QtGui.QToolButton(self.tbwTab)
        self.tbtCorner.setText("New tab")
        self.tbtCorner.setCursor(QtCore.Qt.ArrowCursor)
        self.tbtCorner.setAutoRaise(True)
        self.tbwTab.setCornerWidget(self.tbtCorner, QtCore.Qt.TopRightCorner)
        self.tabbar = self.tbwTab.tabBar()
        cornerHeight = self.tbtCorner.height()
        self.tabbar.setMinimumHeight(cornerHeight)
        self.tbtCorner.setMinimumHeight(cornerHeight)
        self.tbwTab.clear()

        self.tbwTab.tabCloseRequested.connect(self.removeTab)
        self.tbtCorner.clicked.connect(self.addTab)

        self.actionLoad.triggered.connect(self.loadwebelementselection)

    @QtCore.Slot(int)
    def removeTab(self, index):
        """ remove a given tab by its index
        """
        self.tbwTab.removeTab(index)

    @QtCore.Slot()
    def addTab(self):
        """ add a new tab
        """
        tabcount = self.tbwTab.count()
        tabname = "Project {}".format(tabcount + 1)
        aiowidget = allinonewidget.QAllInOneWidget(self.tbwTab)
        # connect signals and slots
        aiowidget.ChangeTabName.connect(self.changeTabName)
        aiowidget.RestoreTabName.connect(self.changeTabName)

        # add the tab
        index = self.tbwTab.addTab(aiowidget, tabname)
        aiowidget.setName(tabname)
        aiowidget.setIndex(index)

    @QtCore.Slot()
    def loadwebelementselection(self):
        """ calls the method of CustomQWebView to load a selection file
        """
        currwid = self.tbwTab.currentWidget()
        if currwid:
            # check what pages is set
            if currwid.stacked.currentIndex() == 1:
                stkwid = currwid.stacked.currentWidget()
                stkwid.wvbScraper.loadselectionfromfile()

    @QtCore.Slot(int, unicode)
    def changeTabName(self, index, newname):
        self.tbwTab.setTabText(index, newname)
