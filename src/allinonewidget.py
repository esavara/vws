#!/usr/bin/env python2
# coding: utf-8
#                        VENI, SANCTE SPIRITUS

from PySide import QtGui
from PySide import QtCore
from . import pagewidgets
from itertools import cycle

"""This file defines a QStackedWidget widget with the menu,

the QWebView for the API and the CustomQWebView for scraping
websites.
"""


class QAllInOneWidget(QtGui.QWidget):

    """ The widget containing the QStackedWidget
    """

    ChangeTabName = QtCore.Signal(int, str)
    RestoreTabName = QtCore.Signal(int, str)

    # Data for the scrap website widget have its QWebView loading a website
    nowloadingcycle = cycle([u" ... Now Loading ... ",
                             u" ·.. Now Loading ·.. ",
                             u" .·. Now Loading .·. ",
                             u" ..· Now Loading ..· "])

    def __init__(self, parent=None):
        super(QAllInOneWidget, self).__init__(parent)
        self.grid = QtGui.QGridLayout(self)
        self.grid.setContentsMargins(0, 0, 0, 0)

        self.stacked = QtGui.QStackedWidget(self)
        self.grid.addWidget(self.stacked, 0, 0, 1, 1)

        self.menu = pagewidgets.MenuUi(self.stacked)
        self.scrap = pagewidgets.ScrapUi(self.stacked)
        self.api = pagewidgets.APIUi(self.stacked)

        self.stacked.addWidget(self.menu)
        self.stacked.addWidget(self.scrap)
        self.stacked.addWidget(self.api)

        self.stacked.setCurrentIndex(0)
        self.menu.pbtWebsite.clicked.connect(self.pbtWebsite_clicked)
        self.menu.pbtAPI.clicked.connect(self.pbtAPI_clicked)

        # tab index and default name
        self.tabindex = None
        self.tabname = None

        # timer to change the tab name
        self.timer = QtCore.QTimer(self)
        self.timer.setInterval(100)
        self.timer.timeout.connect(self.timer_finished)

        # connect the custom QWebView with the timer
        self.scrap.wvbScraper.loadStarted.connect(self.timer.start)
        self.scrap.wvbScraper.loadFinished.connect(self.loadFinish)

    @QtCore.Slot()
    def pbtWebsite_clicked(self):
        """ When the first button on the menu is pressed changes the page
        """
        self.stacked.setCurrentIndex(1)

    @QtCore.Slot()
    def pbtAPI_clicked(self):
        """ When the second button on the menu is pressed change the page
        """
        self.stacked.setCurrentIndex(2)

    @QtCore.Slot()
    def loadFinish(self):
        """ When the QWebView finished loading, restore the tab's name
        """
        self.RestoreTabName.emit(self.tabindex, self.tabname)
        self.timer.stop()

    @QtCore.Slot()
    def timer_finished(self):
        """ When the timer has timeout
        """
        self.ChangeTabName.emit(self.tabindex, next(self.nowloadingcycle))

    def setName(self, name):
        self.tabname = name

    def setIndex(self, index):
        self.tabindex = index
