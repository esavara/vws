#!/usr/bin/env python2
# coding: utf-8
#                        VENI, SANCTE SPIRITUS

from ui import websites_list_dialog_ui
from PySide import QtGui
from PySide import QtCore


class WebsitesDialog(QtGui.QDialog, websites_list_dialog_ui.Ui_Dialog):

    nolinksleft = QtCore.Signal()

    def __init__(self, parent=None):
        super(WebsitesDialog, self).__init__(parent=parent)
        self.setupUi(self)
        self.move(QtGui.QApplication.desktop().screen()
                  .rect().center() - self.rect().center())

        self.pbtClear.clicked.connect(self.ltwLinks.clear)

    @QtCore.Slot()
    def on_pbtAdd_clicked(self):
        """ Add the link in lneLink widget
        """
        if self.lneLink.text() != "":
            link = self.lneLink.text()
            self.ltwLinks.addItem(QtGui.QListWidgetItem(link))

    @QtCore.Slot()
    def on_pbtDelete_clicked(self):
        """ Deletes a link from the list of Websites
        """
        curritem = self.ltwLinks.currentItem()
        if curritem:
            item = self.ltwLinks.takeItem(
                self.ltwLinks.indexFromItem(curritem))

    @QtCore.Slot()
    def on_pbtFile_clicked(self):
        """ The user wants to load the list of links from a file
        """
        filelocation, extension = QtGui.QFileDialog.getOpenFileName(
            self, "Load links from file", "", "TXT(*.txt)")
        if filelocation:
            qfile = QtCore.QFile(filelocation, self)
            if not qfile.open(QtCore.QIODevice.ReadOnly | QtCore.QIODevice.Text):
                # FIXME: an error message to the user is better?
                return

            out = QtCore.QTextStream(qfile)
            while not out.atEnd():
                line = qfile.readLine()
                if not line.isEmpty():
                    url = QtCore.QUrl(str(line))
                    if url.isValid():
                        item = QtGui.QListWidgetItem(str(url.toEncoded()))
                        self.ltwLinks.addItem(item)

    def getnexturl(self):
        """ Take an item and return a QUrl
        """
        item = self.ltwLinks.takeItem(0)
        if item is None:
            self.nolinksleft.emit()
            return None
        else:
            return QtCore.QUrl(item.text())
