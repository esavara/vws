#!/usr/bin/env python2
# coding: utf-8

"""
Esta porción de código, basado en
http://www.riverbankcomputing.com/pipermail/pyqt/2009-May/022961.html
nos permite atajar cualquier excepción sin manejar y mostrarla en una caja
de dialogo al usuario.
"""

from PySide import QtGui
from PySide.QtCore import Qt
import cStringIO
import traceback
import logging
import pprint
from os.path import join, expanduser

"""
importa este modulo en tu archivo principal
y asigna esta función a sys.excepthook.

import sys
sys.excepthook = pyside_excephook.excepthook
"""


def excepthook(excType, excValue, tracebackobj):
    """
    @param excType exception type
    @param excValue exception value
    @param tracebackobj traceback object
    """
    tbinfostream = cStringIO.StringIO()
    traceback.print_tb(tracebackobj, None, tbinfostream)
    tbinfostream.seek(0)
    tbinfostring = tbinfostream.read()
    errmsg = 'Type: "%s".\n Value: "%s"\n\n' % (repr(excType), str(excValue))

    # creamos la caja de mensaje
    errorbox = QtGui.QMessageBox()
    errorbox.setTextFormat(Qt.RichText)
    errorbox.setText(
        "An unhandled exception occurred. Please report it.")
    errorbox.setInformativeText(
        "Contact the original developer"
        " and <b>attach the file located at {}</b> "
        "Don't try to re-run the application until "
        "you attach that file in your message.".format(
            join(expanduser("~"), "Visual Web Scraper",
                 "Debug", "scraper.log")))

    logging.info("Error information: {}".format(
        "\n".join([errmsg, tbinfostring])))

    logging.info(pprint.pprint(locals()))
    errorbox.setIcon(QtGui.QMessageBox.Critical)
    errorbox.setWindowTitle("Uh Oh!")
    errorbox.setStandardButtons(QtGui.QMessageBox.Ok)
    errorbox.exec_()
