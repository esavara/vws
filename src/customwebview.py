#!/usr/bin/env python2
# -*- coding: utf-8 -*-
#                        VENI, SANCTE SPIRITUS
import json
from datetime import datetime
from os.path import expanduser, join

import unicodecsv
from PySide import QtCore, QtGui
from PySide.QtWebKit import QWebView

import logging
logger = logging.getLogger(__name__)

from . import webelementinfo
from . import webframeinfo


class ProxyBoolean(QtCore.QObject):

    """ This class allows us to know wheter a element inside
    the DOM is visible/existent or not
    """
    isvisible = True

    @QtCore.Slot()
    def isVisible(self):
        return self.isvisible

    @QtCore.Slot(bool)
    def setVisibility(self, result):
        self.isvisible = result

    @QtCore.Slot()
    def reset(self):
        self.isvisible = True


class CustomQWebView(QWebView):

    """ A custom QWebView with point-n-click capabilities
    """
    # Signals
    makeCommentBoxButtonAvailable = QtCore.Signal(bool)
    makeAllButtonsAvailable = QtCore.Signal(bool)
    makeStartButtonAvailable = QtCore.Signal(bool)
    startscrap = QtCore.Signal()  # begins the scrap
    finishscrap = QtCore.Signal()  # stops the scrap at user request
    pagescraped = QtCore.Signal()  # tells if the actual page has been scraped

    text1 = "SCRAPING..."
    text2 = "This process may take several minutes, please wait."

    proxyobj = ProxyBoolean()

    def __init__(self, parent=None):
        """
        :param parent: A `QObject` object as parent
        :type parent: QObject
        """
        super(CustomQWebView, self).__init__(parent=parent)
        # File descriptor
        self.writefile = None
        # CSV file for writing
        self.csvwriter = None

        self._children = {}
        self.rememberthese = {}
        self.element = None
        self.selectingelement = False
        self.selecttype = None
        self.scraping = False
        self.actualhost = None

        self.pagination_links_to_visit = []
        self.pagination_links_visited = []

        self.loadFinished.connect(self.pageloadfinished)
        self.pagescraped.connect(self.on_pagescraped)
        # A timer for stoping the load of the page when timeout
        self.timeouttimer = QtCore.QTimer(self)
        self.timeouttimer.setInterval(45 * 1000)
        self.timeouttimer.setSingleShot(True)

        # connect the timer with the widget
        self.timeouttimer.timeout.connect(self.stop)
        self.loadFinished.connect(self.timeouttimer.stop)
        self.loadStarted.connect(self.timeouttimer.start)
        self.mainframe.javaScriptWindowObjectCleared.connect(self.proxyboolean)

        self.load("https://duckduckgo.com")

    @property
    def mainframe(self):
        """The main frame of the website
        :return: QWebFrame
        :rtype: QWebFrame

        .. note::
            This is implemented like a `@property` to workaround the PySide's ownership issue.
        """
        return self.page().mainFrame()

    @property
    def viewport(self):
        """
        :return: The visible part of the `QWebView`
        :rtype: QRect
        """
        return self.page().viewportSize()

    @QtCore.Slot()
    def proxyboolean(self):
        """Set a new `ProxyBoleean` object
        """
        self.mainframe.addToJavaScriptWindowObject(
            "vwsproxyobj", self.proxyobj)
        self.proxyobj.reset()

    def setchild(self, name, webelement):
        """ Set a `WebElement` object as child of this `CustomQWebView`
        :param name: Name of the element
        :type name: str
        :param webelement: the child `WebElement`
        :type webelement: WebElement
        """
        self._children[str(name)] = webelement

    def getchild(self, name):
        """ Get `WebElement` by name
        :param name: The name of the object
        :type name: str
        :return: The wished object, else `False`
        :rtype: WebElement, bool
        """
        if str(name) in self._children:
            return self._children[str(name)]
        else:
            return False

    def removechild(self, name):
        """Remove a child element by name
        :param name: The name of the object
        :type name: str
        :return: `True` if deleted, else `False` if didn't exists
        :rtype: bool
        """
        if str(name) in self._children:
            self._children.pop(str(name))
            return True
        else:
            return False

    def removeallchildren(self):
        """Remove all children from this `CustomQWebView`
        """
        self._children.clear()

    @QtCore.Slot()
    def stopSelection(self):
        """Stop the selection step
        """
        logger.info(
            "Stopping selection for object type {}".format(self.selecttype))
        self.selectingelement = False
        self.selecttype = None
        self.color = None
        self.element = None

    @QtCore.Slot()
    def on_pbtStop_clicked(self):
        """Stop the scrap step
        """
        self.scraping = False
        self.scraptimer.stop()
        logger.info("Scrap stopped by the user")
        self.finishscrap.emit()

    @QtCore.Slot(bool)
    def selectCommentsArea(self, checked):
        """ For selecting the comment area

        :param checked: The check status for the widget
        :type checked: bool
        """
        if checked:
            self.setup_rectcolor_area(0)
        else:
            self.removeallchildren()
            self.update()

    @QtCore.Slot(bool)
    def selectCommentBox(self, checked):
        """ For selecting the comment box

        :param checked: The check status for the widget
        :type checked: bool
        """
        if checked:
            self.setup_rectcolor_area(1)
        else:
            cmma = self.getchild("commentsarea")
            if cmma:
                cmma.removeallchildren()
                self.update()

    @QtCore.Slot(bool)
    def selectUsername(self, checked):
        """ For selecting the commentator's user name

        :param checked: The check status for the widget
        :type checked: bool
        """
        if checked:
            self.setup_rectcolor_area(2)
        else:
            cmma = self.getchild("commentsarea")
            if cmma:
                cmma.removechildinchildren("author")
                self.update()

    @QtCore.Slot(bool)
    def selectDatetime(self, checked):
        """ For selecting the comment's date and time

        :param checked: The check status for the widget
        :type checked: bool
        """
        if checked:
            self.setup_rectcolor_area(3)
        else:
            cmma = self.getchild("commentsarea")
            if cmma:
                cmma.removechildinchildren("datetime")
                self.update()

    @QtCore.Slot(bool)
    def selectCommentText(self, checked):
        """ For selecting the comment's text

        :param checked: The check status for the widget
        :type checked: bool
        """
        if checked:
            self.setup_rectcolor_area(4)
        else:
            cmma = self.getchild("commentsarea")
            if cmma:
                cmma.removechildinchildren("text")
                self.update()

    @QtCore.Slot(bool)
    def selectClickme(self, checked):
        """For selecting that element that should be clicked many times

        :param checked: The check status for the widget
        :type checked: bool
        """
        if checked:
            self.setup_rectcolor_area(5)
        else:
            self.removechild("clickme")
            self.update()

    @QtCore.Slot(bool)
    def selectionPagination(self, checked):
        """For selecting that element that contains links to other pages

        :param checked: The check status for the widget
        :type checked: bool
        """
        if checked:
            self.setup_rectcolor_area(6)
        else:
            self.removechild("pagination")
            self.update()

    @QtCore.Slot(QtGui.QResizeEvent)
    def resizeEvent(self, event):
        """Update the rect of each child if the window's size changed

        :param checked: The check status for the widget
        :type checked: bool
        """
        super(CustomQWebView, self).resizeEvent(event)
        for element in self._children.itervalues():
            element.updaterect()

    @QtCore.Slot(QtGui.QMouseEvent)
    def mousePressEvent(self, event):
        """When the mouse press event happens

        :param event: The event object
        :type event: QEvent
        .. note::
            When scraping or selecting a element, the method do not pass any click to the website
        """
        if self.selectingelement:
            self.selectingelement = not self.selectingelement
            self.selecttype = None
            self.update()
        elif self.scraping:
            # if scraping, don't pass the click event
            return
        else:
            super(CustomQWebView, self).mousePressEvent(event)

    @QtCore.Slot(QtGui.QMouseEvent)
    def mouseMoveEvent(self, event):
        """When the mouse move event happens

        :param event: The event object
        :type event: QEvent
        .. note::
            Many important things happen here, read the source code for details
        """
        if self.selectingelement:
            # consider the scroll position too!
            poswithscroll = event.pos() + \
                self.mainframe.scrollPosition()
            # look for the tag inside the child frames first
            element = self.childframes.getelementbypos(
                poswithscroll, self.selecttype)
            if not element:
                # Not there? Look in the main frame then
                element = self.getelementbypos(poswithscroll, self.selecttype)
                if not element:
                    return

            comar = None
            combx = None
            if self.selecttype <= 4:
                # Ask for these two elements, as we use them frequently
                comar = self.getchild("commentsarea")
                # This may return None, but not big deal since we are using it
                # between select type 2 and 4
                if comar:
                    combx = comar.getchild("commentbox_1")
                    if not combx:
                        # if there is not comment box set yet,
                        # set the variable to a empty list
                        combx = []

            if self.selecttype == 0:
                # Comments Area is being set
                if element != comar:
                    self.setchild("commentsarea", element)
            elif self.selecttype == 1:
                # Comment boxes are being set
                if element not in comar:
                    comar.removeallchildren()
                    comar.setchildren(
                        "commentbox", element, element.getcsspath(), 1)
            elif self.selecttype == 2:
                # the author of the comment is being set to every comment box
                if element not in combx:
                    comar.setchildtochildren("author", element.getcsspath(), 2)
            elif self.selecttype == 3:
                # the date and time of a comment is being set to every comment
                # box
                if element not in combx:
                    comar.setchildtochildren(
                        "datetime", element.getcsspath(), 3)
            elif self.selecttype == 4:
                # the text of the comment is being set to every comment box
                if element not in combx:
                    comar.setchildtochildren("text", element.getcsspath(), 4)
            elif self.selecttype == 5:
                # click me element is being set
                clickme = self.getchild("clickme")
                if element != clickme:
                    self.setchild("clickme", element)
            elif self.selecttype == 6:
                # pagination element is being set
                pagination = self.getchild("pagination")
                if element != pagination:
                    self.setchild("pagination", element)

            self.update()
        elif not self.scraping:
            super(CustomQWebView, self).mouseMoveEvent(event)

    @QtCore.Slot(QtGui.QPaintEvent)
    def paintEvent(self, event):
        """Time to paint the widget!

        :param event: The event object
        :type event: QEvent
        """
        # draw the content first
        super(CustomQWebView, self).paintEvent(event)

        # main painter
        painter = QtGui.QPainter(self)

        for child in self._children.itervalues():
            child.draw(painter)

        # Paint everything black if scraping
        if self.scraping:
            # Paint a semi-transparent black rectangle with the text telling
            # the user a scraping is taking place and he need to wait before
            # interacting with the website again
            blackrect = QtCore.QRect(
                0, 0, self.viewport.width(), self.viewport.height())
            painter.fillRect(blackrect, QtGui.QColor(0, 0, 0, 200))
            painter.setPen(QtGui.QPen(QtCore.Qt.white))

            vpcenterh = self.viewport.width() / 2
            vpcenterv = self.viewport.height() / 2

            qfont = QtGui.QFont("sans", 50, QtGui.QFont.Bold)
            fpm = QtGui.QFontMetrics(qfont)

            qfont2 = QtGui.QFont("sans", 16)
            fsm = QtGui.QFontMetrics(qfont2)

            fpwidth = fpm.width(self.text1)
            fpheight = fpm.height()

            fswidth = fsm.width(self.text2)
            fsheight = fsm.height()

            painter.setFont(qfont)
            painter.drawText(vpcenterh - fpwidth / 2,
                             vpcenterv - fpheight / 2,
                             fpwidth, fpheight,
                             QtCore.Qt.AlignHCenter, self.text1)
            painter.setFont(qfont2)
            painter.drawText(vpcenterh - fswidth / 2,
                             (vpcenterv + fpheight / 2) - fsheight / 2,
                             fswidth, fsheight,
                             QtCore.Qt.AlignHCenter, self.text2)

    def updateframe(self):
        """Update the list of child frames and they tag inventory
        """
        self.childframes = webframeinfo.WebFrames(self)
        self.childframes.maketagsinventory()
        self.maketagsinventory()

    def pageloadfinished(self):
        self.updateframe()

        parent = self.parent()
        if parent.action_step_next_page:
            if parent.pbtStart.isEnabled() is not True and \
               parent.pbtStop.isEnabled() is True:
                # We are in the middle of a scraping process
                self.makepagesinventory()

        if self.scraping:
            self.temporallyloadselectedelements()
            self.startscraping()

        logger.info("Page load finished")

    def setup_rectcolor_area(self, forarea):
        """Called when we want to select certain area of a web site

        This method set-up the painter to a giving color so web elements are
        drawn with a rect on top. Also activates the flag to allow painting
        inside `CustomQWebView`.

        :param forarea: For which area we are going to set the painter
        :type forarea: int
        """
        self.selectingelement = True

        # defines what we are looking to select
        self.selecttype = forarea
        logger.info(
            "Setting rectangle color for element type {}".format(forarea))

    @QtCore.Slot()
    def startscraping(self):
        """ Set and start the scraping process

        .. note::
            Read the source code for details
        """
        # check for what steps are marked as enabled, and if they are, perform
        # them
        logger.info("Scrap started by the user")
        self.scraping = True
        # No focus please
        self.setFocusPolicy(QtCore.Qt.NoFocus)

        if not self.writefile:
            # open a file for writing data on it
            domainname = self.url().host()
            if domainname == u"":
                domainname = u"unknown-host"

            domainname = domainname.replace(".", "_")

            timenow = datetime.now()

            filename = "{}-{}-output.csv".format(
                domainname,
                timenow.strftime("%Y%m%d-%H%M%S"))

            self.writefile = open(
                join(expanduser("~"), "Visual Web Scraper",
                     "Scrap output", filename), "w")
            fields = ["Author", "Date", "Comment", "Website"]
            self.csvwriter = unicodecsv.DictWriter(
                self.writefile, fieldnames=fields, restval="")
            self.csvwriter.writeheader()

        if not self.actualhost:
            # Set any value
            self.actualhost = 1

        if self.actualhost != self.url().host():
            # The first time actualhost will hold a different value, which is
            # 1, so setting the url host here is a good idea
            self.actualhost = self.url().host()
            # Save the data here
            self.saveselectiontofile()

        parent = self.parent()
        parent.on_scrapstarted()
        if parent.action_step_next_page.isChecked():
            if not self.pagination_links_to_visit and \
               not self.pagination_links_visited:
                self.makepagesinventory()

        if parent.action_step_scroll.isChecked():
            # we need to scroll down until we reach the bottom of the page
            self.scrolldownrepeatedly()
            parent.action_step_scroll.setChecked(False)
            self.updateselectedelements()

        if parent.action_step_click.isChecked():
            # something needs to be clicked
            self.clickthisrepeatedly()
            parent.action_step_click.setChecked(False)
            self.updateselectedelements()

        self.scraptimer = QtCore.QTimer()
        self.scraptimer.timeout.connect(self.on_scraptimer_timeout)
        self.scraptimer.setInterval(200)

        cmmas = self.getchild("commentsarea")
        self.commentboxsgen = cmmas._children.itervalues()
        self.scraptimer.start()

    @QtCore.Slot()
    def stopscraping(self):
        """Stop the scraping process
        """
        try:
            self.scraptimer.stop()
        except AttributeError:
            pass
        self.pagination_links_to_visit = []
        self.pagination_links_visited = []
        self.scraping = False
        # Get focus again
        self.setFocusPolicy(QtCore.Qt.StrongFocus)
        self.csvwriter = None
        self.writefile.close()
        self.writefile = None
        self.actualhost = None
        title = "Scrap stopped"
        text = "The scrap process has been stopped by the user"
        self.update()
        QtGui.QMessageBox.information(self, title, text)

    def updateselectedelements(self):
        """When more elements are load, re-select them all
        """
        commentsarea = self.getchild("commentsarea")
        commentbox = commentsarea.getchild("commentbox_1")

        author = commentbox.getchild("author")
        if author:
            author = author.getcsspath()

        date = commentbox.getchild("datetime")
        if date:
            date = date.getcsspath()

        text = commentbox.getchild("text")
        if text:
            text = text.getcsspath()

        commentsarea.removeallchildren()
        # populate the web element again
        commentsarea.setchildren(
            "commentbox", commentbox, commentbox.getcsspath(), 1)

        if author:
            commentsarea.setchildtochildren("author", author, 2)

        if date:
            commentsarea.setchildtochildren("datetime", date, 3)

        if text:
            commentsarea.setchildtochildren("text", text, 4)

    @QtCore.Slot()
    def on_scraptimer_timeout(self):
        """ Process and save the data gathered
        """
        try:
            commentbox = self.commentboxsgen.next()
            authorchild = commentbox.getchild("author")
            datechild = commentbox.getchild("datetime")
            textchild = commentbox.getchild("text")

            if authorchild:
                author = authorchild.web_element.toPlainText().strip(
                    "\n").strip()
            else:
                author = ""

            if datechild:
                datetimes = datechild.web_element.toPlainText().strip(
                    "\n").strip()
            else:
                datetimes = ""

            if textchild:
                text = textchild.web_element.toPlainText().strip()
            else:
                text = ""

            if author or text:
                drow = {"Author": author,
                        "Date": datetimes,
                        "Comment": text,
                        "Website": self.url().toEncoded()}
                if self.csvwriter:
                    self.csvwriter.writerow(drow)
        except StopIteration:
            # Stop the timer here, it will be resume if more pages needs to be
            # scraped
            self.scraptimer.stop()
            self.pagescraped.emit()

    @QtCore.Slot()
    def on_pagescraped(self):
        """Called when a link is scraped

        This first add the scraped link into the list of visited links.  then
        gets the next link. if no new links left for scraping, looks for the
        next link in the main list of links. If no more links left there, the
        signal `CustomQWebView.finishscrap` is emitted.

        """
        oldlink = self.url()
        self.pagination_links_visited.append(unicode(oldlink.toEncoded()))
        try:
            # next page!
            nextpage = self.pagination_links_to_visit.pop(0)
            self.load(nextpage)
        except IndexError:
            url = self.parent().dlgWebsites.getnexturl()
            if url:
                self.pagination_links_visited.append(unicode(url.toEncoded()))
                self.load(url)
            else:
                self.pagination_links_to_visit = []
                self.pagination_links_visited = []
                self.scraping = False
                self.csvwriter = None
                if self.writefile:
                    self.writefile.close()
                self.writefile = None
                self.actualhost = None
                title = "Scrap finished!"
                text = "The scrap process has finished successfully"
                QtGui.QMessageBox.information(self, title, text)
                parentwidget = self.parent()
                parentwidget.action_comments_area.setChecked(False)
                self.removeallchildren()

    def maketagsinventory(self):
        """Do a inventory of all HTML tags for a web page and sort every element by size.

        .. note::
            The sorting puts the tiny tags first.
        """
        self.boxes = []
        frames = self.mainframe.childFrames()
        if frames:
            frames.append(self.mainframe)
        else:
            frames = [self.mainframe]

        for frame in frames:
            self.boxes.extend(frame.findAllElements("*").toList())
        # remove some elements. Those with no width and height and those who
        # are invalid
        for child in self.boxes:
            if child.isNull():
                self.boxes.remove(child)

        # sort list from the most tiny element to the biggest
        self.boxes = sorted(self.boxes, key=lambda tag: tag.geometry().width()
                            * tag.geometry().height(), reverse=False)
        logger.info("Tags inventory done")

    @QtCore.Slot()
    def makepagesinventory(self):
        """Get all links from pagination elements

        removes those links already visited and already scheduled to visit
        """
        pagination = self.getchild("pagination")
        collection = pagination.web_element.findAll("a")
        # FIXME: check that those links aren't relative links
        links = [a.attribute("href") for a in collection]
        # remove links already visited
        links = list(set(links) - set(self.pagination_links_visited))
        # remove links already scheduled to visit
        links = list(set(links) - set(self.pagination_links_to_visit))
        # finally, add the remaining links to schedule for visit
        self.pagination_links_to_visit.extend(links)

    def getelementbypos(self, pos, selecttype):
        """ return a WebElement which is under the actual position of the cursor

        :param pos: Position of the mouse
        :type pos: QPoint
        :param selecttype: The id for the element returned
        :type selecttype: int
        """
        child = None
        for findchild in self.boxes:
            if findchild.geometry().contains(pos.x(), pos.y()):
                child = findchild
                break

        if child:
            return webelementinfo.WebElement(child, selecttype, parent=self)

    def isVisible(self, element):
        """ Return True if the element is visible

        :param element: The element to check
        :type element: WebElement, QWebElement
        """
        self.proxyobj.reset()
        try:
            element.web_element.evaluateJavaScript(
                "var style = window.getComputedStyle(this);"
                " vwsproxyobj.setVisibility(style.display !== 'none');")
        except AttributeError:
            element.evaluateJavaScript(
                "var style = window.getComputedStyle(this);"
                " vwsproxyobj.setVisibility(style.display !== 'none');")
        finally:
            return self.proxyobj.isVisible()

    def clickthis(self, element):
        """Click the given element using Javascript

        :param element: The element to click
        :type element: WebElement, QWebElement
        """
        # Do the click
        try:
            element.web_element.evaluateJavaScript("this.click()")
        except AttributeError:
            element.evaluateJavaScript("this.click()")

        # scroll down as well
        barheight = self.mainframe.scrollBarMaximum(QtCore.Qt.Vertical)
        scrollpos = self.mainframe.scrollPosition()
        newpos = QtCore.QPoint(scrollpos.x(), scrollpos.y() + barheight)
        self.mainframe.setScrollPosition(newpos)

        self.updateselectedelements()

    def clickthisrepeatedly(self):
        """ Click the given element using JavaScript until it is gone or invisible
        """
        element = self.getchild("clickme")
        css = element.getcsspath()
        then = datetime.now()
        eventloop = QtCore.QEventLoop(self.parent())
        lock = False

        while self.isVisible(element):
            eventloop.processEvents()
            if not self.scraping:
                break

            if not lock:
                self.clickthis(element)
                lock = not lock

            period = datetime.now() - then
            if period.total_seconds() >= 5.0:
                then = datetime.now()
                try:
                    element = element.frame.findFirstElement(css)
                except AttributeError:
                    element = element.webFrame().findFirstElement(css)

                lock = not lock

        self.removechild("clickme")
        self.update()

    def scrolldownrepeatedly(self):
        """ Scroll down the website
        """
        then = datetime.now()
        eventloop = QtCore.QEventLoop(self.parent())
        stop = False
        while not stop and self.scraping:
            eventloop.processEvents()
            barheight = self.mainframe.scrollBarMaximum(
                QtCore.Qt.Vertical)
            scrollpos = self.mainframe.scrollPosition()
            period = datetime.now() - then
            if scrollpos.y() != barheight and period.total_seconds() >= 5.0:
                # we haven't reach the bottom!
                then = datetime.now()
                newpos = QtCore.QPoint(
                    scrollpos.x(), scrollpos.y() + barheight)
                self.mainframe.setScrollPosition(newpos)
            elif scrollpos.y() == barheight and period.total_seconds() >= 10.0:
                # seems like we reached the bottom!
                stop = True
            elif not self.scraping:
                break

    @QtCore.Slot(QtCore.QUrl)
    def load(self, url):
        """ re-implementation of the load method

        Simply call the temporallysaveselectedelements
        before trying to load the url
        :param url: The URL
        :param url: QUrl, str
        """
        self.temporallysaveselectedelements()
        self.removeallchildren()
        self.update()
        logger.info("Loading {}...".format(url))
        super(CustomQWebView, self).load(url)

    def temporallysaveselectedelements(self):
        """ Store the CSS path of each selected element
        """
        if self._children:
            logger.info("Temporally saving current selection")
            self.rememberthese = self.getasdict()

    def saveselectiontofile(self):
        """ Save the selection made by the user for the current domain
        """
        self.temporallysaveselectedelements()
        hostname = self.url().host()
        if not hostname:
            hostname = self.url().path()
            hostname = hostname.replace("/", "-")

        hostname = hostname.replace(".", "_")

        logger.info("Saving current selection for '{}'".format(hostname))

        filename = "{}-elementselection.json".format(hostname)
        with open(
                join(expanduser("~"), "Visual Web Scraper",
                     "Selections", filename), "w") as esf:
            json.dump(self.rememberthese, esf,
                      indent=2, separators=(",", ": "),
                      sort_keys=True)

    def loadselectionfromfile(self):
        """ Load a saved selection made by the user for the current domain
        """
        homedir = join(expanduser("~"), "Visual Web Scraper",
                       "Selections")

        filename = QtGui.QFileDialog.getOpenFileName(
            self, caption="Load selection of elements from previous sessions",
            dir=homedir, filter="JSON(*.json)")[0]
        if filename:
            logger.info("Loading a selection file from '{}'".format(homedir))
            with open(filename) as esf:
                self.rememberthese = json.load(esf)
                self.temporallyloadselectedelements()

    def str2objs(self, string):
        """ Deserialize web elements and they children
        """
        path = str(string).split(":")
        if len(path) < 3:
            raise ValueError(
                "String path malformed/invalid: {}".format(string))
        else:
            frame = self.childframes.getframebyname(
                path[0])
            if not frame:
                frame = self.mainframe

            stype = int(path[1])

            wb = frame.findFirstElement(path[2])
            webel = webelementinfo.WebElement(wb, stype)
            return webel

    def temporallyloadselectedelements(self):
        """ Deserialize web elements and their children
        """
        if self.rememberthese:
            logger.info("Temporally loading saved selection")
            # Comments Area
            parentwidget = self.parent()
            if "commentsarea" in self.rememberthese:
                commas = self.str2objs(
                    self.rememberthese["commentsarea"]["csspath"])
                self.setchild("commentsarea", commas)
                parentwidget.action_comments_area.setEnabled(True)
                parentwidget.action_comments_area.setChecked(True)
                self.stopSelection()

                # comment boxs
                if "commentbox_0" in self.rememberthese[
                        "commentsarea"]["children"]:
                    commbx = self.str2objs(
                        self.rememberthese["commentsarea"][
                            "children"]["commentbox_0"]["csspath"])
                    commas.setchildren(
                        "commentbox", commbx, commbx.getcsspath(), 1)
                    parentwidget.action_comment_box.setEnabled(True)
                    parentwidget.action_comment_box.setChecked(True)
                    self.stopSelection()

                    try:
                        # author in comment box
                        author = self.str2objs(
                            self.rememberthese["commentsarea"]["children"][
                                "commentbox_0"][
                                    "children"]["author"]["csspath"])
                        commas.setchildtochildren(
                            "author", author.getcsspath(), 2)
                        parentwidget.action_author.setEnabled(True)
                        parentwidget.action_author.setChecked(True)
                        self.stopSelection()
                        parentwidget.reloadqcss()
                    except KeyError:
                        pass

                    try:
                        # date and time in comment box
                        date = self.str2objs(
                            self.rememberthese["commentsarea"][
                                "children"]["commentbox_0"]["children"][
                                    "datetime"]["csspath"])
                        commas.setchildtochildren(
                            "datetime", date.getcsspath(), 3)
                        parentwidget.action_datetime.setEnabled(True)
                        parentwidget.action_datetime.setChecked(True)
                        self.stopSelection()
                        parentwidget.reloadqcss()
                    except KeyError:
                        pass

                    try:
                        # text in comment box
                        text = self.str2objs(
                            self.rememberthese["commentsarea"][
                                "children"]["commentbox_0"][
                                    "children"]["text"]["csspath"])
                        commas.setchildtochildren(
                            "text", text.getcsspath(), 4)
                        parentwidget.action_comment_text.setEnabled(True)
                        parentwidget.action_comment_text.setChecked(True)
                        self.stopSelection()
                        parentwidget.reloadqcss()
                    except KeyError:
                        pass

            try:
                # "click me" element in web page
                clickme = self.str2objs(
                    self.rememberthese["clickme"]["csspath"])
                self.setchild("clickme", clickme)
                parentwidget.action_step_click.setEnabled(True)
                parentwidget.action_step_click.setChecked(True)
                self.stopSelection()
            except KeyError:
                pass

            try:
                # pagination container in web page
                pag = self.str2objs(
                    self.rememberthese["pagination"]["csspath"])
                self.setchild("pagination", pag)
                parentwidget.action_step_next_page.setEnabled(True)
                parentwidget.action_step_next_page.setChecked(True)
            except KeyError:
                pass

            self.update()
            return True
        else:
            return False

    def getasdict(self):
        """Serialize web elements and their children
        """
        myself = {}
        if self._children:
            for name, obj in self._children.iteritems():
                myself[name] = obj.getasdict()

        return myself
