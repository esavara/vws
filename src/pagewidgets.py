#!/usr/bin/env python2
# coding: utf-8
#                        VENI, SANCTE SPIRITUS

from ui import menu_widget_ui
from ui import api_widget_ui
from ui import scrapwebsite_widget_ui
from PySide import QtGui
from PySide import QtCore
from re import search
from os import getenv

from . import customwebview
from . import websitesdialog

import logging

logger = logging.getLogger(__name__)


"""These widgets may be under two or three levels of parents"""


class MenuUi(QtGui.QWidget, menu_widget_ui.Ui_Form):

    def __init__(self, parent=None):
        super(MenuUi, self).__init__(parent)
        self.setupUi(self)


class APIUi(QtGui.QWidget, api_widget_ui.Ui_Form):

    def __del__(self):
        if hasattr(self, "webappprocess"):
            self.webappprocess.close()

    def __init__(self, parent=None):
        super(APIUi, self).__init__(parent)
        self.setupUi(self)
        userhomedir = QtCore.QDir.home()
        # Location of the main web server of the plugin system
        webapppath = userhomedir.absoluteFilePath(
            "Visual Web Scraper/Plugins/server/mainwebapp.exe")
        if userhomedir.exists(webapppath):
            logger.info("File {} exists".format(webapppath))
            self.webappprocess = QtCore.QProcess(self)
            self.webappprocess.readyReadStandardOutput.connect(
                self.outputdatatoread)
            self.webappprocess.waitForStarted(-1)
            self.webappprocess.waitForFinished(-1)
            logger.info("Executing {}...".format(webapppath))
            self.webappprocess.start(webapppath, [])
        else:
            title = "Oh no!"
            text = ("The plugin server do not exists! (and it should) Please"
                    " reinstall the application to solve this issue")
            logger.info("File {} do not exists".format(webapppath))
            if not getenv("WERCKER_DOING_TESTS", None):
                QtGui.QMessageBox.critical(self, title, text)

    @QtCore.Slot()
    def outputdatatoread(self):
        """ Called to read the Stdout after the APP is called
        """
        logger.info("Output from Stdout present!")
        output = unicode(self.webappprocess.readAllStandardOutput())
        logger.info("Output: {}".format(output))
        lines = output.split("\n")
        line = lines[0]
        appipaddr = line.split(" ")[-1].strip()
        logger.info(
            "The main web app plugin is listening at: {}".format(appipaddr))
        self.wbvAPI.load("http://127.0.0.1" + appipaddr[appipaddr.rfind(":"):])
        # disconnect the signal and this slot
        self.webappprocess.readyReadStandardOutput.disconnect(
            self.outputdatatoread)
        logger.debug("Signal readyReadStandardOutput disconnected")


class ScrapUi(QtGui.QWidget, scrapwebsite_widget_ui.Ui_Form):

    def __init__(self, parent=None):
        super(ScrapUi, self).__init__(parent=parent)
        self.setupUi(self)
        self.dlgWebsites = websitesdialog.WebsitesDialog(self)
        self.pbtWebsites.clicked.connect(self.dlgWebsites.exec_)

        self.wvbScraper = customwebview.CustomQWebView(self)
        self.wvbScraper.setObjectName("wvbScraper")
        self.gridLayout.addWidget(self.wvbScraper, 2, 0, 1, 1)

        self.wvbScraper.urlChanged.connect(self.wvbScraper_url_changed)
        self.dlgWebsites.nolinksleft.connect(self.wvbScraper.finishscrap)
        self.wvbScraper.finishscrap.connect(self.on_scrapstopped)
        self.wvbScraper.startscrap.connect(self.on_scrapstarted)

        self.pbtNext.clicked.connect(self.wvbScraper.forward)
        self.pbtPrevious.clicked.connect(self.wvbScraper.back)
        self.pbtRefresh.clicked.connect(self.wvbScraper.reload)
        self.pbtBrowserStop.clicked.connect(self.wvbScraper.stop)
        self.lneURL.returnPressed.connect(self.gotourl)
        self.pbtStop.clicked.connect(self.wvbScraper.stopscraping)
        self.pbtStop.clicked.connect(self.on_scrapstopped)

        # add contextual menus
        self.menumark = QtGui.QMenu(self)
        self.menustep = QtGui.QMenu(self)

        # add options for menumark
        self.menumark.addAction(self.action_comments_area)
        self.menumark.addAction(self.action_comment_box)
        self.menumark.addSeparator()
        self.menumark.addAction(self.action_author)
        self.menumark.addAction(self.action_datetime)
        self.menumark.addAction(self.action_comment_text)

        # add options for menustep
        # self.menustep.addAction(self.action_step_open_iframe)
        self.menustep.addAction(self.action_step_scroll)
        self.menustep.addAction(self.action_step_click)
        self.menustep.addAction(self.action_step_next_page)

        # add both menus to their respective buttons
        self.pbtMark.setMenu(self.menumark)
        self.pbtSteps.setMenu(self.menustep)

        # connecting the buttons signal to the wvbScraper slots to enable the
        # selection of tags in web pages
        self.action_comments_area.toggled.connect(
            self.wvbScraper.selectCommentsArea)
        self.action_comment_box.toggled.connect(
            self.wvbScraper.selectCommentBox)
        self.action_comment_text.toggled.connect(
            self.wvbScraper.selectCommentText)
        self.action_datetime.toggled.connect(self.wvbScraper.selectDatetime)
        self.action_author.toggled.connect(self.wvbScraper.selectUsername)

        # connect other slots with some actions' signals
        self.action_comments_area.toggled.connect(
            self.on_selectCommentsArea_toggle)
        self.action_comment_box.toggled.connect(
            self.on_selectCommentBox_toggle)
        self.action_author.toggled.connect(self.on_anyelement_selected)
        self.action_comment_text.toggled.connect(self.on_anyelement_selected)
        self.action_datetime.toggled.connect(self.on_anyelement_selected)

        self.pbtStart.clicked.connect(self.wvbScraper.startscraping)

        # Connect some action steps with some slots
        self.action_step_click.toggled.connect(self.wvbScraper.selectClickme)
        self.action_step_next_page.toggled.connect(
            self.wvbScraper.selectionPagination)

        self.wvbScraper.loadStarted.connect(self.on_loadstarted)
        self.wvbScraper.loadFinished.connect(self.on_loadfinished)

        self.lneURL.setText("about:blank")

    @QtCore.Slot(QtCore.QUrl)
    def wvbScraper_url_changed(self, url):
        """ update the text on lneURL when the url changes on wvbScraper
        """
        self.lneURL.setText(url.toString())

    @QtCore.Slot()
    def gotourl(self):
        """ When the user press return on lneURL, we go to that website
        """
        text = self.lneURL.text()
        if not search(r'(ht+ps?|file):/+', text):
            text = u"http://" + text
            self.lneURL.setText(text)

        self.wvbScraper.setUrl(QtCore.QUrl(self.lneURL.text()))

    @QtCore.Slot(bool)
    def on_selectCommentsArea_toggle(self, checked):
        """ Enable the comment box action or disables it
        """
        self.action_comment_box.setEnabled(checked)

        if not checked:
            self.action_comment_box.toggled.emit(checked)
            self.action_comment_box.setChecked(checked)
            # Disable and uncheck the rest of actions too
            self.on_selectCommentBox_toggle(checked)

    @QtCore.Slot()
    def on_selectCommentBox_toggle(self, checked):
        """ Enable the rest of actions action or disables it
        """
        self.action_author.setEnabled(checked)
        self.action_comment_text.setEnabled(checked)
        self.action_datetime.setEnabled(checked)

        if not checked:
            self.action_author.toggled.emit(checked)
            self.action_comment_text.toggled.emit(checked)
            self.action_datetime.toggled.emit(checked)

            self.action_author.setChecked(checked)
            self.action_comment_text.setChecked(checked)
            self.action_datetime.setChecked(checked)

    @QtCore.Slot()
    def on_anyelement_selected(self):
        """ Enable the start button if some of these elements are enable,
        or disable it if any of these elements are disabled.
        """
        state = int(self.action_author.isChecked()) +\
            int(self.action_datetime.isChecked()) +\
            int(self.action_comment_text.isChecked())
        state = bool(state)
        self.pbtStart.setEnabled(state)
        self.reloadqcss()

    @QtCore.Slot()
    def on_loadstarted(self):
        """ When the browser has started the loading, disable buttons
        """
        if not self.wvbScraper.scraping:
            self.pbtMark.setEnabled(False)
            self.pbtSteps.setEnabled(False)
            self.action_author.setChecked(False)
            self.action_comment_box.setChecked(False)
            self.action_comment_text.setChecked(False)
            self.action_comments_area.setChecked(False)
            self.action_datetime.setChecked(False)
            self.action_step_click.setChecked(False)
            self.action_step_next_page.setChecked(False)
            self.action_step_scroll.setChecked(False)

            self.wvbScraper.stopSelection()

        self.reloadqcss()

    @QtCore.Slot()
    def on_loadfinished(self):
        """ When the browser has finished the loading, enable buttons
        """
        if not self.wvbScraper.scraping:
            self.pbtMark.setEnabled(True)
            self.pbtSteps.setEnabled(True)
        self.reloadqcss()

    @QtCore.Slot()
    def on_scrapstarted(self):
        """ Disable some buttons when the scrap is started
        """
        self.pbtMark.setEnabled(False)
        self.pbtStart.setEnabled(False)
        self.pbtStop.setEnabled(True)
        self.pbtSteps.setEnabled(False)
        self.reloadqcss()

    @QtCore.Slot()
    def on_scrapstopped(self):
        """ Enable some buttons when the scraper has been stopped
        """
        self.pbtMark.setEnabled(True)
        self.pbtStart.setEnabled(True)
        self.pbtStop.setEnabled(False)
        self.pbtSteps.setEnabled(True)
        self.reloadqcss()

    @QtCore.Slot()
    def reloadqcss(self):
        """ Reload the style
        """
        QtCore.QCoreApplication.instance().setStyleSheet(
            QtCore.QCoreApplication.instance().styleSheet())
        self.setStyleSheet(QtCore.QCoreApplication.instance().styleSheet())
