#!/usr/bin/env python2
# coding: utf-8
#                        VENI, SANCTE SPIRITUS

from PySide import QtGui
from PySide import QtCore
from PySide.QtWebKit import QWebElement
import re

import logging
from hashlib import sha256
logger = logging.getLogger(__name__)


class WebElement(QtCore.QObject):

    """Information about elements selected in the web page.

    This is for extending the `QWebElement` object by composition and to
    gain new features required by this project.
    """

    def __contains__(self, value):
        """
        :param value: a `WebElement` or `QWebElement` or any other object
        :return: `True` if element is child of this element
        :rtype: bool
        """
        for element in self._children.itervalues():
            if value == element:
                return True

            return False

    def __eq__(self, other):
        """
        :param other: a `WebElement` or `QWebElement` or any other object
        :return: `True` if a element is equal to this element
        :rtype: bool
        """
        if isinstance(other, WebElement):
            return self.web_element == other.web_element
        elif isinstance(other, QWebElement):
            return self.web_element == other
        else:
            return False

    def __ne__(self, other):
        """
        :param other: a `WebElement` or `QWebElement` or any other object
        :return: `True` if a element is not equal to this element
        :rtype: bool
        """
        if isinstance(other, WebElement):
            return self.web_element != other.web_element
        elif isinstance(other, QWebElement):
            return self.web_element != other
        else:
            return True

    def __nonzero__(self):
        """Tests truth value of the object

        :return: `True` if QWebElement is `True`, else `False`
        :rtype: bool
        """
        if self.web_element:
            return True
        else:
            return False

    def __init__(self, webelement, selectiontype, parent=None):
        """
        :param webelement: a `QWebElement`
        :param selectiontype: a `int` value between 0 and 6.
        :param parent: The parent QObject for `WebElement`

        .. note::
            About *param selectiontype* it indicate the type of the selected element.

            - 0 is for Comment Area: The area where the comments are.
            - 1 is for Comment box: the tiny area containing the interesting data.
            - 2 is for Author: the author name of the comment.
            - 3 is for Date and Time: The date and Time when the comment was made.
            - 4 is for Comment text: The commentary text itself.
            - 5 is for Click me: a element that should be clicked many times in order to load more comments from the page.
            - 6 is for Pagination: The area holding the next pages of comments.
        """
        super(WebElement, self).__init__(parent)

        if webelement is None:
            raise ValueError(
                "Argument 'webelement' is None")

        if not isinstance(selectiontype, int):
            raise ValueError(
                "Argument 'selectiontype' is {}, expected int".format(
                    type(selectiontype)))

        self.colors = {0: QtGui.QColor(255, 165, 0, 127),  # Comment Area
                       1: QtGui.QColor(135, 206, 235, 127),  # Comment box
                       2: QtGui.QColor(135, 235, 164, 127),  # Author
                       3: QtGui.QColor(235, 135, 206, 127),  # date and time
                       4: QtGui.QColor(235, 164, 135, 127),  # comment text
                       5: QtGui.QColor(204, 0, 0, 127),  # click me
                       6: QtGui.QColor(255, 255, 0, 127)}  # pagination

        if hasattr(webelement, "element"):
            self.web_element = webelement.element()
        else:
            self.web_element = webelement

        # child WebElement of this WebElement
        self._parent = None
        self._children = {}
        # A list of WebElements like this one
        self.otherslikeme = []

        if hasattr(webelement, "boundingRect"):
            rect = webelement.boundingRect()
        else:
            rect = webelement.geometry()

        self.element_rect_x = rect.x()
        self.element_rect_y = rect.y()
        self.element_rect_w = rect.width()
        self.element_rect_h = rect.height()

        self.elementtype = selectiontype
        self.color = self.colors[self.elementtype]
        self.color_darker = self.color.darker()
        self.pen = QtGui.QPen(self.color_darker)
        self.pen.setWidth(3)

    @property
    def frame(self):
        """
        :return: the `QWebFrame` of the current `QWebElement`
        :rtype: `QWebFrame`

        .. note::
            This is implemented like a `@property` to workaround the PySide's ownership issue.
        """
        return self.web_element.webFrame()

    @property
    def frame_scroll_x(self):
        """
        :return: the x axis position of the scroll for the frame of the current `QWebElement`.
        :rtype: int

        .. note::
            This is implemented like a @property to workaround the PySide's ownership issue.
        """
        return self.web_element.webFrame().scrollPosition().x()

    @property
    def frame_scroll_y(self):
        """
        :return: the y axis position of the scroll for the frame of the current `QWebElement`.
        :rtype: int

        .. note::
            This is implemented like a @property to workaround the PySide's ownership issue.
        """
        return self.web_element.webFrame().scrollPosition().y()

    def setparentwebelement(self, parent):
        """Set the `WebElement` parent for the current WebElement.

        :param parent: The `WebElement` to use as parent
        :return: None
        :rtype: None.
        """
        if not isinstance(parent, WebElement):
            raise ValueError(
                "Argument passed is {}, expected WebElement type".format(
                    type(parent)))
        else:
            self._parent = parent

    def setchild(self, name, webelement):
        """Set a `WebElement` object as child of this `WebElement`.

        :param name: Name of the child
        :param webelement: the `WebElement` to use as child
        :return: None
        :rtype: None
        """
        webelement.setparentwebelement(self)
        logger.debug("Child {} set".format(name))
        self._children[str(name)] = webelement

    def getchild(self, name):
        """
        :param name: The name of the child
        :return: The `WebElement` child by its name.
        :rtype: WebElement
        """
        if str(name) in self._children:
            return self._children[str(name)]
        else:
            return False

    def removechild(self, name):
        """Remove a child from this `WebElement`

        :return: `False` if object did not exists, else returns `True` if deleted.
        :rtype: bool    
        """
        logger.debug("Removing child {}".format(name))
        if str(name) in self._children:
            self._children.pop(str(name))
            return True
        else:
            return False

    def removeallchildren(self):
        """Remove all children from this `WebElement`

        :return: None
        :rtype: None
        """
        self._children.clear()

    def childexists(self, name):
        """Check if child exists

        :param name: The name of the child to check
        :type name: str
        :return: `True` if exists, else `False`
        :rtype: bool
        """
        if str(name) in self._children:
            return True
        else:
            return False

    def setchildren(self, basename, element, csspath, selecttype):
        """Set children as child of this `WebElement`

        :param basename: The base name
        :param element: The element to set as child
        :param csspath: The CSS selector
        :param selecttype: The number id to identify the child as
        :type basename: str
        :type element: QWebElement
        :type csspath: str
        :type selecttype: int
        :return: None
        :rtype: None
        """

        try:
            children = element.frame.findAllElements(csspath).toList()
        except AttributeError:
            children = element.webFrame().findAllElements(csspath).toList()

        for element in children:
            index = children.index(element)
            if not element.isNull():
                we = WebElement(element, selecttype)
                self.setchild("{}_{}".format(basename, index), we)

    def setchildtochildren(self, name, csspath, selecttype):
        """To each child of this element set a child in them

        :param name: name for each child
        :type name: str
        :param csspath: CSS selector of the element to use as child
        :type csspath: str
        :param selecttype: The number id to identify the child as
        :type selecttype: int
        :return: None
        :rtype: None
        """
        for element in self._children.itervalues():
            qwebel = element.web_element.findFirst(csspath)
            if not qwebel.isNull():
                we = WebElement(qwebel, selecttype)
                element.setchild(name, we)

    def removechildinchildren(self, name):
        """To each child of this element remove the child in them

        :param name: The name of the element to remove
        :type name: str
        :return: None
        :rtype: None
        """
        for element in self._children.itervalues():
            element.removechild(name)

    def getpaintobjs(self, justrect=False):
        """Return things useful for drawing this element

        :param justrect: Tell the method to return just the rectangle of the element
        :type justrect: bool
        :return: A `dict` with a `QRectF`, `QColor` and `QPen`
        :rtype: dict
        """
        rect = self.getrect()
        rectf = QtCore.QRectF(rect)
        if justrect is not False:
            return rectf
        else:
            return {"rect": rectf,
                    "color": self.color,
                    "pen": self.pen}

    def getrect(self):
        """
        :return: Return the `QRect` for this `WebElement`
        :rtype: QRect
        """
        if self.frame.parentFrame() is None:
            rect = QtCore.QRect()
            rect.setRect(
                self.element_rect_x - self.frame_scroll_x,
                self.element_rect_y - self.frame_scroll_y,
                self.element_rect_w, self.element_rect_h)
        else:
            parentframe = self.frame.parentFrame()
            rect = QtCore.QRect()

            rect.setRect(
                (self.element_rect_x + self.frame.pos().x()) -
                parentframe.scrollPosition().x(),
                (self.element_rect_y + self.frame.pos().y()) -
                parentframe.scrollPosition().y(),
                self.element_rect_w, self.element_rect_h)

        return rect

    def updaterect(self):
        """Updates the rect size

        :return: None
        :rtype: None
        """
        rect = self.web_element.geometry()
        self.element_rect_x = rect.x()
        self.element_rect_y = rect.y()
        self.element_rect_w = rect.width()
        self.element_rect_h = rect.height()
        # update children elements too
        for child in self._children.itervalues():
            child.updaterect()

    @staticmethod
    def _buildpathfragment(element, ignoreclassid, fbframe, dsqframe):
        """Makes the fragment of the path selector

        :param element: The webelement
        :type element: WebElement or QWebElement
        :param ignoreclassid: Tells the method to always ignore both the class and id of the element
        :type ignoreclassid: bool
        :param fbframe: Tells if the element is inside a iframe of Facebook comments
        :type fbframe: bool
        :param dsqframe: Tells if the element is inside a iframe of Disqus comments
        :type dsqframe: bool
        :return: a fragment of a CSS selector based on the given element
        :rtype: str

        .. note::
            both `fbframe` and `dsqframe` are workarounds to allow
            the application to allow selecting multiple elements with
            different ids. Nevertheless, this don't works for Disqus
            and I would recommend doing a plugin for API Scraper instead.
        """
        tagname = element.tagName().lower()

        # FIXME: This doesn't work with disqus comments, better create a plugin
        # for API Scraper.
        if element.attribute("id", None) and not ignoreclassid and \
                (not fbframe or not dsqframe):
            tagname = tagname + "#" + \
                element.attribute("id")

        if element.attribute("class", None) and not ignoreclassid:
            classsplit = element.attribute("class").split(" ")
            classfragment = ".".join(classsplit)
            tagname = tagname + "." + classfragment

        return tagname

    def getcsspath(self):
        """Build the CSS selector for this element

        :return: CSS selector
        :rtype: str
        """
        pathlist = []
        myself = self.web_element
        # when choosing any element but commet box that
        # is it inside a frame of Facebook comments?
        fbframe = "facebook.com" in self.frame.url(
        ).toEncoded() and self.elementtype >= 2
        # is the comment box inside a frame of Disqus comments?
        dsqframe = "disqus.com" in self.frame.url(
        ).toEncoded() and self.elementtype == 1
        while not myself.isNull():
            if not pathlist and self.elementtype != 1:
                # We are working on the last element, the selected element that
                # is. Ask for the parent web element of the actual web element
                myself_parentelement = myself.parent()
                selectors = []
                # get the first child of such parent
                sibling = myself_parentelement.firstChild()
                # while the sibling element is not equal to the actual element,
                # navigate all the siblings

                while sibling != myself:
                    if not sibling.isNull():
                        fragment = self._buildpathfragment(
                            sibling, True, False, False)
                        selectors.append(fragment)

                    sibling = sibling.nextSibling()
                else:
                    fragment = self._buildpathfragment(
                        sibling, False, False, dsqframe)
                    selectors.append(fragment)

                pathfragment = " + ".join(selectors)
                pathfragment = "> " + pathfragment

                pathlist.append(pathfragment)
            elif not pathlist and self.elementtype == 1:
                fragment = self._buildpathfragment(
                    myself, False, False, dsqframe)
                pathlist.append("> " + fragment)
            else:
                # This is another element, not the actual one selected by the
                # user, maybe the parent or grand parent, etc.
                fragment = self._buildpathfragment(
                    myself, False, fbframe, dsqframe)
                pathlist.append(fragment)

            # goes up one element
            myself = myself.parent()

        # The last element was inserted first, this is wrong
        pathlist.reverse()
        path = " ".join(pathlist)
        # normalize the path
        # remove any repeated dot
        path = re.sub(r"\.+", ".", path)
        # remove any dot at the end of each path fragment
        path = re.sub(r"\. ", "", path)

        return str(path)

    def getasdict(self):
        """
        :return: Return a dictionary with the CSS selector of the current element and its children. Useful for writing a JSON file later.
        :rtype: dict
        """
        myself = {}
        framename = "<main>"
        # check if the frame have a name, otherwise we are dealing with the
        # main frame
        if self.frame.frameName():
            framename = sha256(self.frame.frameName()).hexdigest()

        myself["csspath"] = "{}:{}:{}".format(
            framename, self.elementtype, self.getcsspath())
        if self._children:
            # if we have children, include them too
            myself["children"] = {}
            if self.elementtype == 0:
                # The children are comment boxes, so we only pick one!
                child = self._children["commentbox_0"]
                myself["children"]["commentbox_0"] = child.getasdict()
            else:
                for name, obj in self._children.iteritems():
                    myself["children"][name] = obj.getasdict()

        return myself

    def isNull(self):
        """Check if element is null

        :return: True if this element is Null
        :rtype: bool
        """
        return self.web_element.isNull()

    def draw(self, painter):
        """ Draw a rect around the `WebElement`, This is called inside a `PaintEvent`.

        :param painter: The `QPaint` object
        :return: None
        :rtype: None
        """
        painter.setPen(self.pen)
        painter.drawRect(self.getrect())
        for child in self._children.itervalues():
            child.draw(painter)
