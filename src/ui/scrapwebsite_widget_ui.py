# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ui/scrapwebsite_widget.ui'
#
# Created: Fri Jul 10 00:20:22 2015
#      by: pyside-uic 0.2.15 running on PySide 1.2.2
#
# WARNING! All changes made in this file will be lost!

from PySide import QtCore, QtGui

class Ui_Form(object):
    def setupUi(self, Form):
        Form.setObjectName("Form")
        Form.resize(650, 480)
        self.gridLayout = QtGui.QGridLayout(Form)
        self.gridLayout.setContentsMargins(0, 0, 0, 0)
        self.gridLayout.setObjectName("gridLayout")
        self.horizontalLayout = QtGui.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.pbtWebsites = QtGui.QPushButton(Form)
        self.pbtWebsites.setMaximumSize(QtCore.QSize(150, 16777215))
        self.pbtWebsites.setText("Websites list")
        self.pbtWebsites.setObjectName("pbtWebsites")
        self.horizontalLayout.addWidget(self.pbtWebsites)
        self.line_2 = QtGui.QFrame(Form)
        self.line_2.setFrameShape(QtGui.QFrame.VLine)
        self.line_2.setFrameShadow(QtGui.QFrame.Sunken)
        self.line_2.setObjectName("line_2")
        self.horizontalLayout.addWidget(self.line_2)
        self.pbtPrevious = QtGui.QPushButton(Form)
        self.pbtPrevious.setMaximumSize(QtCore.QSize(32, 16777215))
        self.pbtPrevious.setText("")
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(":/icons/arrowhead7.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.pbtPrevious.setIcon(icon)
        self.pbtPrevious.setIconSize(QtCore.QSize(16, 16))
        self.pbtPrevious.setObjectName("pbtPrevious")
        self.horizontalLayout.addWidget(self.pbtPrevious)
        self.pbtNext = QtGui.QPushButton(Form)
        self.pbtNext.setMaximumSize(QtCore.QSize(32, 16777215))
        self.pbtNext.setText("")
        icon1 = QtGui.QIcon()
        icon1.addPixmap(QtGui.QPixmap(":/icons/arrow487.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.pbtNext.setIcon(icon1)
        self.pbtNext.setIconSize(QtCore.QSize(16, 16))
        self.pbtNext.setObjectName("pbtNext")
        self.horizontalLayout.addWidget(self.pbtNext)
        self.lneURL = QtGui.QLineEdit(Form)
        self.lneURL.setObjectName("lneURL")
        self.horizontalLayout.addWidget(self.lneURL)
        self.pbtRefresh = QtGui.QPushButton(Form)
        self.pbtRefresh.setMaximumSize(QtCore.QSize(32, 16777215))
        self.pbtRefresh.setText("")
        icon2 = QtGui.QIcon()
        icon2.addPixmap(QtGui.QPixmap(":/icons/arrows64.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.pbtRefresh.setIcon(icon2)
        self.pbtRefresh.setIconSize(QtCore.QSize(16, 16))
        self.pbtRefresh.setObjectName("pbtRefresh")
        self.horizontalLayout.addWidget(self.pbtRefresh)
        self.pbtBrowserStop = QtGui.QPushButton(Form)
        self.pbtBrowserStop.setMaximumSize(QtCore.QSize(32, 16777215))
        self.pbtBrowserStop.setText("")
        icon3 = QtGui.QIcon()
        icon3.addPixmap(QtGui.QPixmap(":/icons/delete30.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.pbtBrowserStop.setIcon(icon3)
        self.pbtBrowserStop.setIconSize(QtCore.QSize(16, 16))
        self.pbtBrowserStop.setObjectName("pbtBrowserStop")
        self.horizontalLayout.addWidget(self.pbtBrowserStop)
        self.gridLayout.addLayout(self.horizontalLayout, 0, 0, 1, 1)
        self.horizontalLayout_2 = QtGui.QHBoxLayout()
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.pbtStart = QtGui.QPushButton(Form)
        self.pbtStart.setEnabled(False)
        self.pbtStart.setObjectName("pbtStart")
        self.horizontalLayout_2.addWidget(self.pbtStart)
        self.pbtStop = QtGui.QPushButton(Form)
        self.pbtStop.setEnabled(False)
        self.pbtStop.setObjectName("pbtStop")
        self.horizontalLayout_2.addWidget(self.pbtStop)
        self.line = QtGui.QFrame(Form)
        self.line.setFrameShape(QtGui.QFrame.VLine)
        self.line.setFrameShadow(QtGui.QFrame.Sunken)
        self.line.setObjectName("line")
        self.horizontalLayout_2.addWidget(self.line)
        self.pbtMark = QtGui.QPushButton(Form)
        self.pbtMark.setObjectName("pbtMark")
        self.horizontalLayout_2.addWidget(self.pbtMark)
        self.pbtSteps = QtGui.QPushButton(Form)
        self.pbtSteps.setObjectName("pbtSteps")
        self.horizontalLayout_2.addWidget(self.pbtSteps)
        spacerItem = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout_2.addItem(spacerItem)
        self.gridLayout.addLayout(self.horizontalLayout_2, 1, 0, 1, 1)
        self.action_comments_area = QtGui.QAction(Form)
        self.action_comments_area.setCheckable(True)
        self.action_comments_area.setObjectName("action_comments_area")
        self.action_comment_box = QtGui.QAction(Form)
        self.action_comment_box.setCheckable(True)
        self.action_comment_box.setEnabled(False)
        self.action_comment_box.setObjectName("action_comment_box")
        self.action_author = QtGui.QAction(Form)
        self.action_author.setCheckable(True)
        self.action_author.setEnabled(False)
        self.action_author.setObjectName("action_author")
        self.action_datetime = QtGui.QAction(Form)
        self.action_datetime.setCheckable(True)
        self.action_datetime.setEnabled(False)
        self.action_datetime.setObjectName("action_datetime")
        self.action_comment_text = QtGui.QAction(Form)
        self.action_comment_text.setCheckable(True)
        self.action_comment_text.setEnabled(False)
        self.action_comment_text.setObjectName("action_comment_text")
        self.action_step_scroll = QtGui.QAction(Form)
        self.action_step_scroll.setCheckable(True)
        self.action_step_scroll.setObjectName("action_step_scroll")
        self.action_step_click = QtGui.QAction(Form)
        self.action_step_click.setCheckable(True)
        self.action_step_click.setObjectName("action_step_click")
        self.action_step_next_page = QtGui.QAction(Form)
        self.action_step_next_page.setCheckable(True)
        self.action_step_next_page.setObjectName("action_step_next_page")
        self.action_step_open_iframe = QtGui.QAction(Form)
        self.action_step_open_iframe.setCheckable(True)
        self.action_step_open_iframe.setObjectName("action_step_open_iframe")

        self.retranslateUi(Form)
        QtCore.QMetaObject.connectSlotsByName(Form)

    def retranslateUi(self, Form):
        Form.setWindowTitle(QtGui.QApplication.translate("Form", "Form", None, QtGui.QApplication.UnicodeUTF8))
        self.pbtPrevious.setToolTip(QtGui.QApplication.translate("Form", "Go back in the browser history", None, QtGui.QApplication.UnicodeUTF8))
        self.pbtNext.setToolTip(QtGui.QApplication.translate("Form", "Go forward in the browser history", None, QtGui.QApplication.UnicodeUTF8))
        self.lneURL.setToolTip(QtGui.QApplication.translate("Form", "URL", None, QtGui.QApplication.UnicodeUTF8))
        self.pbtRefresh.setToolTip(QtGui.QApplication.translate("Form", "Refresh the website", None, QtGui.QApplication.UnicodeUTF8))
        self.pbtBrowserStop.setToolTip(QtGui.QApplication.translate("Form", "Stop loading the website", None, QtGui.QApplication.UnicodeUTF8))
        self.pbtStart.setToolTip(QtGui.QApplication.translate("Form", "Start scraping", None, QtGui.QApplication.UnicodeUTF8))
        self.pbtStart.setText(QtGui.QApplication.translate("Form", "Start", None, QtGui.QApplication.UnicodeUTF8))
        self.pbtStop.setToolTip(QtGui.QApplication.translate("Form", "Stop scraping", None, QtGui.QApplication.UnicodeUTF8))
        self.pbtStop.setText(QtGui.QApplication.translate("Form", "Stop", None, QtGui.QApplication.UnicodeUTF8))
        self.pbtMark.setToolTip(QtGui.QApplication.translate("Form", "Mark web page elements by type", None, QtGui.QApplication.UnicodeUTF8))
        self.pbtMark.setText(QtGui.QApplication.translate("Form", "Mark element", None, QtGui.QApplication.UnicodeUTF8))
        self.pbtSteps.setToolTip(QtGui.QApplication.translate("Form", "Add extra steps for scraping, i.e.: click ceratins element for loading more content or passing to the next page of comments", None, QtGui.QApplication.UnicodeUTF8))
        self.pbtSteps.setText(QtGui.QApplication.translate("Form", "Scraping steps", None, QtGui.QApplication.UnicodeUTF8))
        self.action_comments_area.setText(QtGui.QApplication.translate("Form", "As comments area", None, QtGui.QApplication.UnicodeUTF8))
        self.action_comments_area.setToolTip(QtGui.QApplication.translate("Form", "The area where all comments are", None, QtGui.QApplication.UnicodeUTF8))
        self.action_comment_box.setText(QtGui.QApplication.translate("Form", "As comment box", None, QtGui.QApplication.UnicodeUTF8))
        self.action_comment_box.setToolTip(QtGui.QApplication.translate("Form", "The box that represents a comment element", None, QtGui.QApplication.UnicodeUTF8))
        self.action_author.setText(QtGui.QApplication.translate("Form", "As author\'s name", None, QtGui.QApplication.UnicodeUTF8))
        self.action_author.setToolTip(QtGui.QApplication.translate("Form", "The box that contains just the comment author\'s name", None, QtGui.QApplication.UnicodeUTF8))
        self.action_datetime.setText(QtGui.QApplication.translate("Form", "As time and date", None, QtGui.QApplication.UnicodeUTF8))
        self.action_datetime.setToolTip(QtGui.QApplication.translate("Form", "The box that contains just the comment\'s time and date", None, QtGui.QApplication.UnicodeUTF8))
        self.action_comment_text.setText(QtGui.QApplication.translate("Form", "As the comment\'s text", None, QtGui.QApplication.UnicodeUTF8))
        self.action_comment_text.setToolTip(QtGui.QApplication.translate("Form", "The box that contains just the comment\'s text", None, QtGui.QApplication.UnicodeUTF8))
        self.action_step_scroll.setText(QtGui.QApplication.translate("Form", "Scroll down", None, QtGui.QApplication.UnicodeUTF8))
        self.action_step_scroll.setToolTip(QtGui.QApplication.translate("Form", "Make the browser scroll down making the website load more content", None, QtGui.QApplication.UnicodeUTF8))
        self.action_step_click.setText(QtGui.QApplication.translate("Form", "Click element", None, QtGui.QApplication.UnicodeUTF8))
        self.action_step_click.setToolTip(QtGui.QApplication.translate("Form", "Click an element that cause the website to load more content", None, QtGui.QApplication.UnicodeUTF8))
        self.action_step_next_page.setText(QtGui.QApplication.translate("Form", "Get other pages", None, QtGui.QApplication.UnicodeUTF8))
        self.action_step_next_page.setToolTip(QtGui.QApplication.translate("Form", "Mark the box of pagination, when the web page is scraped the scraper will visit the next page", None, QtGui.QApplication.UnicodeUTF8))
        self.action_step_open_iframe.setText(QtGui.QApplication.translate("Form", "Open frame/iframe", None, QtGui.QApplication.UnicodeUTF8))
        self.action_step_open_iframe.setToolTip(QtGui.QApplication.translate("Form", "If the box marked as comments area is an iframe/frame tag, the browser will open it before. Useful for sites like YouTube ", None, QtGui.QApplication.UnicodeUTF8))

from . import scrapwidget_rc

if __name__ == "__main__":
    import sys
    app = QtGui.QApplication(sys.argv)
    Form = QtGui.QWidget()
    ui = Ui_Form()
    ui.setupUi(Form)
    Form.show()
    sys.exit(app.exec_())

