# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ui/main.ui'
#
# Created: Fri Jul 10 00:20:22 2015
#      by: pyside-uic 0.2.15 running on PySide 1.2.2
#
# WARNING! All changes made in this file will be lost!

from PySide import QtCore, QtGui

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(640, 480)
        MainWindow.setWindowTitle("Visual Web Scraper")
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(":/icon/app.ico"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        MainWindow.setWindowIcon(icon)
        MainWindow.setStyleSheet("QMainWindow, QDialog { background: #E8E8E8; color: #2e2e2e; }\n"
"\n"
"QMenuBar, QStatusBar {\n"
"    background: #545454;\n"
"    color: #fff;\n"
"}\n"
"\n"
"QLabel#lblBanner {\n"
"    background: qlineargradient(x1: 0, y1: 0.01, x2: 0, y2: 1,\n"
"                                stop: 0 #545454, stop: 1 #141414);\n"
"    margin: 0;\n"
"}\n"
"\n"
"/*QMenu {\n"
"    background: #E8E8E8;\n"
"}*/\n"
"\n"
"QMenuBar::item {\n"
"    border: 1px solid transparent;\n"
"}\n"
"\n"
"QMenuBar::item:selected {\n"
"    background: #444442;\n"
"    border: 0;\n"
"}\n"
"\n"
"QMenuBar::item:pressed {\n"
"    background: #444442;\n"
"}\n"
"\n"
"QMenu::item:selected {\n"
"    background: #cfcfcf;\n"
"}\n"
"\n"
"QGroupBox {\n"
"    background: #fff;\n"
"    border-radius: 5px;\n"
"    margin: 3px 3px 3px 3px;\n"
"    padding: 18px 5px 10px 5px;\n"
"    font-weight: bold;\n"
"}\n"
"\n"
"QGroupBox::title {\n"
"    subcontrol-origin: margin;\n"
"    subcontrol-position: top left;\n"
"    color: black;\n"
"    font-style: bold;\n"
"    margin-left: 6px;\n"
"    margin-top: 7px;\n"
"    background: transparent;\n"
"    color: #2e2e2e;\n"
"}\n"
"\n"
"QPushButton, QDialogButtonBox {\n"
"    background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
"                                stop: 0 #f5f5f5, stop: 1 #cfcfcf); \n"
"    border-radius: 3px;\n"
"    padding: 5px 10px;\n"
"    margin: 1px;\n"
"    min-width: 30px    ;\n"
"}\n"
"\n"
"QPushButton:hover, QDialogButtonBox:hover {\n"
"    background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
"                                stop: 0 #cfcfcf, stop: 1 #b5b5b5);\n"
"}\n"
"\n"
"QPushButton:pressed, QDialogButtonBox:pressed {\n"
"    background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
"                                stop: 0 #b5b5b5, stop: 1 #cfcfcf);\n"
"}\n"
"\n"
"QPushButton:default, QDialogButtonBox:default {\n"
"    border: 1px solid;\n"
"    border-color: orange;\n"
"}\n"
"\n"
"QPushButton[enabled=false]\n"
"{\n"
"    background: #ededed;\n"
"    color: #cfcfcf;\n"
"}\n"
"\n"
"QLineEdit, QTextEdit {\n"
"    background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 0.3,\n"
"                                stop: 0 #f2f2f2, stop: 1 white);\n"
"    border: 2px solid #E8E8E8;\n"
"    selection-background-color: orange;\n"
"    border-radius: 5px;\n"
"    padding: 3px;\n"
"}\n"
"\n"
"QHeaderView::section {\n"
"    background-color: qlineargradient(x1: 0, y1: 0.01, x2: 0, y2: 1,\n"
"                                      stop: 0 #545454, stop: 1 #141414);\n"
"    color: white;\n"
"    padding-left: 10px;\n"
"    border: 1px solid #6c6c6c;\n"
"}\n"
"\n"
"QTreeWidget {\n"
"    border: 2px solid #E8E8E8;\n"
"    border-radius: 5px;\n"
"    selection-background-color: orange;\n"
"    show-decoration-selected: 1;\n"
"}\n"
"\n"
"QTreeWidget::item:selected:!active {\n"
"    background: #e59500;\n"
"}\n"
"\n"
"QTreeWidget::item:selected:active {\n"
"    background: #f29d00;\n"
"}\n"
"\n"
"QTreeWidget::item:hover {\n"
"    background: orange;\n"
"}\n"
"\n"
"QTreeWidget::branch {\n"
"    background: white;\n"
"}\n"
"\n"
"QScrollBar:vertical {\n"
"    background: #fff;\n"
"    width: 10px;\n"
"}\n"
"\n"
"QScrollBar::handle:vertical {\n"
"    background: #E8E8E8;\n"
"}\n"
"\n"
"QScrollBar:horizontal {\n"
"    background: #fff;\n"
"    height: 10px;\n"
"}\n"
"\n"
"QScrollBar::handle:horizontal {\n"
"    background: #E8E8E8;\n"
"}\n"
"\n"
"QProgressBar {\n"
"    border: 2px solid orange;\n"
"    border-radius: 5px;\n"
"    background: #fff;\n"
"    text-align: center;\n"
"}\n"
"\n"
"QProgressBar::chunk {\n"
"    background: orange;\n"
"    margin: 2px;\n"
"    border-radius: 5px;\n"
"}\n"
"\n"
"")
        self.centralwidget = QtGui.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.gridLayout = QtGui.QGridLayout(self.centralwidget)
        self.gridLayout.setObjectName("gridLayout")
        self.tbwTab = QtGui.QTabWidget(self.centralwidget)
        self.tbwTab.setTabsClosable(True)
        self.tbwTab.setMovable(True)
        self.tbwTab.setObjectName("tbwTab")
        self.tab = QtGui.QWidget()
        self.tab.setEnabled(True)
        self.tab.setObjectName("tab")
        self.gridLayout_2 = QtGui.QGridLayout(self.tab)
        self.gridLayout_2.setContentsMargins(0, 0, 0, 0)
        self.gridLayout_2.setObjectName("gridLayout_2")
        self.stwStack = QtGui.QStackedWidget(self.tab)
        self.stwStack.setObjectName("stwStack")
        self.page = QtGui.QWidget()
        self.page.setObjectName("page")
        self.gridLayout_3 = QtGui.QGridLayout(self.page)
        self.gridLayout_3.setObjectName("gridLayout_3")
        self.horizontalLayout = QtGui.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        spacerItem = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem)
        self.verticalLayout = QtGui.QVBoxLayout()
        self.verticalLayout.setObjectName("verticalLayout")
        spacerItem1 = QtGui.QSpacerItem(20, 40, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        self.verticalLayout.addItem(spacerItem1)
        self.pbtWebsite = QtGui.QPushButton(self.page)
        self.pbtWebsite.setAutoDefault(True)
        self.pbtWebsite.setFlat(False)
        self.pbtWebsite.setObjectName("pbtWebsite")
        self.verticalLayout.addWidget(self.pbtWebsite)
        self.pbtAPI = QtGui.QPushButton(self.page)
        self.pbtAPI.setFlat(False)
        self.pbtAPI.setObjectName("pbtAPI")
        self.verticalLayout.addWidget(self.pbtAPI)
        spacerItem2 = QtGui.QSpacerItem(20, 40, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        self.verticalLayout.addItem(spacerItem2)
        self.horizontalLayout.addLayout(self.verticalLayout)
        spacerItem3 = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem3)
        self.gridLayout_3.addLayout(self.horizontalLayout, 0, 0, 1, 1)
        self.stwStack.addWidget(self.page)
        self.page_2 = QtGui.QWidget()
        self.page_2.setObjectName("page_2")
        self.stwStack.addWidget(self.page_2)
        self.gridLayout_2.addWidget(self.stwStack, 0, 1, 1, 1)
        self.tbwTab.addTab(self.tab, "")
        self.tab_2 = QtGui.QWidget()
        self.tab_2.setObjectName("tab_2")
        self.tbwTab.addTab(self.tab_2, "")
        self.gridLayout.addWidget(self.tbwTab, 0, 0, 1, 1)
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtGui.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 640, 20))
        self.menubar.setObjectName("menubar")
        self.menuElement_selections = QtGui.QMenu(self.menubar)
        self.menuElement_selections.setObjectName("menuElement_selections")
        MainWindow.setMenuBar(self.menubar)
        self.actionLoad = QtGui.QAction(MainWindow)
        self.actionLoad.setObjectName("actionLoad")
        self.menuElement_selections.addAction(self.actionLoad)
        self.menubar.addAction(self.menuElement_selections.menuAction())

        self.retranslateUi(MainWindow)
        self.tbwTab.setCurrentIndex(0)
        self.stwStack.setCurrentIndex(0)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        self.pbtWebsite.setText(QtGui.QApplication.translate("MainWindow", "Scrap a website", None, QtGui.QApplication.UnicodeUTF8))
        self.pbtAPI.setText(QtGui.QApplication.translate("MainWindow", "Scrap an API", None, QtGui.QApplication.UnicodeUTF8))
        self.tbwTab.setTabText(self.tbwTab.indexOf(self.tab), QtGui.QApplication.translate("MainWindow", "Tab 1", None, QtGui.QApplication.UnicodeUTF8))
        self.tbwTab.setTabText(self.tbwTab.indexOf(self.tab_2), QtGui.QApplication.translate("MainWindow", "Tab 2", None, QtGui.QApplication.UnicodeUTF8))
        self.menuElement_selections.setTitle(QtGui.QApplication.translate("MainWindow", "Element selection", None, QtGui.QApplication.UnicodeUTF8))
        self.actionLoad.setText(QtGui.QApplication.translate("MainWindow", "Load...", None, QtGui.QApplication.UnicodeUTF8))

from . import mainapp_rc

if __name__ == "__main__":
    import sys
    app = QtGui.QApplication(sys.argv)
    MainWindow = QtGui.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())

