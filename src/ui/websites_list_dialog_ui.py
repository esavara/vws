# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ui/websites_list_dialog.ui'
#
# Created: Fri Jul 10 00:20:22 2015
#      by: pyside-uic 0.2.15 running on PySide 1.2.2
#
# WARNING! All changes made in this file will be lost!

from PySide import QtCore, QtGui

class Ui_Dialog(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.resize(537, 279)
        Dialog.setStyleSheet("QMainWindow, QDialog { background: #E8E8E8; color: #2e2e2e; }\n"
"\n"
"QMenuBar, QStatusBar {\n"
"    background: #545454;\n"
"    color: #fff;\n"
"}\n"
"\n"
"QLabel#lblBanner {\n"
"    background: qlineargradient(x1: 0, y1: 0.01, x2: 0, y2: 1,\n"
"                                stop: 0 #545454, stop: 1 #141414);\n"
"    margin: 0;\n"
"}\n"
"\n"
"QMenu {\n"
"    background: #E8E8E8;\n"
"}\n"
"QMenuBar::item {\n"
"    background: transparent;\n"
"}\n"
"\n"
"QMenuBar::item:selected {\n"
"    background: #444442;\n"
"    border: 0;\n"
"}\n"
"\n"
"QMenuBar::item:pressed {\n"
"    background: #444442;\n"
"}\n"
"\n"
"QMenu::item:selected {\n"
"    background: #cfcfcf;\n"
"}\n"
"\n"
"QGroupBox {\n"
"    background: #fff;\n"
"    /* border: 1px solid white; */\n"
"    border-radius: 5px;\n"
"    margin: 3px 3px 3px 3px;\n"
"    padding: 18px 5px 10px 5px;\n"
"    font-weight: bold;\n"
"}\n"
"\n"
"QGroupBox::title {\n"
"    subcontrol-origin: margin;\n"
"    subcontrol-position: top left;\n"
"    color: black;\n"
"    font-style: bold;\n"
"    /* padding: 4px 5px; */\n"
"    margin-left: 6px;\n"
"    margin-top: 7px;\n"
"    background: transparent;\n"
"    color: #2e2e2e;\n"
"}\n"
"\n"
"QPushButton, QDialogButtonBox {\n"
"    background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
"                                stop: 0 #f5f5f5, stop: 1 #cfcfcf); \n"
"    border-radius: 3px;\n"
"    padding: 5px 10px;\n"
"    margin: 1px;\n"
"    min-width: 30px    ;\n"
"}\n"
"\n"
"QPushButton:hover, QDialogButtonBox:hover {\n"
"    background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
"                                stop: 0 #cfcfcf, stop: 1 #b5b5b5);\n"
"}\n"
"\n"
"QPushButton:pressed, QDialogButtonBox:pressed {\n"
"    background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
"                                stop: 0 #b5b5b5, stop: 1 #cfcfcf);\n"
"}\n"
"\n"
"QPushButton:default, QDialogButtonBox:default {\n"
"    border: 1px solid;\n"
"    border-color: orange; /* make the default button prominent */\n"
"}\n"
"\n"
"QPushButton[enabled=false]\n"
"{\n"
"    background: #ededed;\n"
"    color: #cfcfcf;\n"
"}\n"
"\n"
"QLineEdit, QTextEdit {\n"
"    background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 0.3,\n"
"                                stop: 0 #f2f2f2, stop: 1 white);\n"
"    border: 2px solid #E8E8E8;\n"
"    selection-background-color: orange;\n"
"    border-radius: 5px;\n"
"    padding: 3px;\n"
"}\n"
"\n"
"QHeaderView::section {\n"
"    background-color: qlineargradient(x1: 0, y1: 0.01, x2: 0, y2: 1,\n"
"                                      stop: 0 #545454, stop: 1 #141414);\n"
"    color: white;\n"
"    padding-left: 10px;\n"
"    border: 1px solid #6c6c6c;\n"
"}\n"
"\n"
"QTreeWidget {\n"
"    border: 2px solid #E8E8E8;\n"
"    border-radius: 5px;\n"
"    selection-background-color: orange;\n"
"    show-decoration-selected: 1;\n"
"}\n"
"\n"
"/* QTreeWidget::item:selected { */\n"
"/*     background: ; */\n"
"/* } */\n"
"\n"
"QTreeWidget::item:selected:!active {\n"
"    background: #e59500;\n"
"}\n"
"\n"
"QTreeWidget::item:selected:active {\n"
"    background: #f29d00;\n"
"}\n"
"\n"
"QTreeWidget::item:hover {\n"
"    background: orange;\n"
"}\n"
"\n"
"QTreeWidget::branch {\n"
"    background: white;\n"
"}\n"
"\n"
"/* barra de desplazamiento */\n"
"/* Vertical */\n"
"QScrollBar:vertical {\n"
"    background: #fff;\n"
"    width: 10px;\n"
"}\n"
"\n"
"QScrollBar::handle:vertical {\n"
"    background: #E8E8E8;\n"
"}\n"
"\n"
"/* Horizontal */\n"
"QScrollBar:horizontal {\n"
"    background: #fff;\n"
"    height: 10px;\n"
"}\n"
"\n"
"QScrollBar::handle:horizontal {\n"
"    background: #E8E8E8;\n"
"}\n"
"/* Fin de la barra de desplazamiento */\n"
"\n"
"QProgressBar {\n"
"    border: 2px solid orange;\n"
"    border-radius: 5px;\n"
"    background: #fff;\n"
"    text-align: center;\n"
"}\n"
"\n"
"QProgressBar::chunk {\n"
"    background: orange;\n"
"    margin: 2px;\n"
"    border-radius: 5px;\n"
"}\n"
"")
        Dialog.setModal(True)
        self.gridLayout = QtGui.QGridLayout(Dialog)
        self.gridLayout.setContentsMargins(9, 9, 9, 9)
        self.gridLayout.setObjectName("gridLayout")
        self.lneLink = QtGui.QLineEdit(Dialog)
        self.lneLink.setObjectName("lneLink")
        self.gridLayout.addWidget(self.lneLink, 0, 0, 1, 2)
        self.pbtAdd = QtGui.QPushButton(Dialog)
        self.pbtAdd.setObjectName("pbtAdd")
        self.gridLayout.addWidget(self.pbtAdd, 0, 2, 1, 1)
        self.pbtDelete = QtGui.QPushButton(Dialog)
        self.pbtDelete.setObjectName("pbtDelete")
        self.gridLayout.addWidget(self.pbtDelete, 0, 3, 1, 1)
        self.pbtClear = QtGui.QPushButton(Dialog)
        self.pbtClear.setObjectName("pbtClear")
        self.gridLayout.addWidget(self.pbtClear, 0, 4, 1, 1)
        self.ltwLinks = QtGui.QListWidget(Dialog)
        self.ltwLinks.setObjectName("ltwLinks")
        self.gridLayout.addWidget(self.ltwLinks, 1, 0, 1, 5)
        self.pbtFile = QtGui.QPushButton(Dialog)
        self.pbtFile.setObjectName("pbtFile")
        self.gridLayout.addWidget(self.pbtFile, 2, 0, 1, 1)
        self.buttonBox = QtGui.QDialogButtonBox(Dialog)
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtGui.QDialogButtonBox.Close)
        self.buttonBox.setObjectName("buttonBox")
        self.gridLayout.addWidget(self.buttonBox, 3, 0, 1, 5)
        spacerItem = QtGui.QSpacerItem(406, 26, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.gridLayout.addItem(spacerItem, 2, 1, 1, 4)

        self.retranslateUi(Dialog)
        QtCore.QObject.connect(self.buttonBox, QtCore.SIGNAL("accepted()"), Dialog.accept)
        QtCore.QObject.connect(self.buttonBox, QtCore.SIGNAL("rejected()"), Dialog.reject)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        Dialog.setWindowTitle(QtGui.QApplication.translate("Dialog", "Websites list", None, QtGui.QApplication.UnicodeUTF8))
        self.pbtAdd.setText(QtGui.QApplication.translate("Dialog", "Add link", None, QtGui.QApplication.UnicodeUTF8))
        self.pbtDelete.setText(QtGui.QApplication.translate("Dialog", "Delete link", None, QtGui.QApplication.UnicodeUTF8))
        self.pbtClear.setText(QtGui.QApplication.translate("Dialog", "Clear all", None, QtGui.QApplication.UnicodeUTF8))
        self.pbtFile.setText(QtGui.QApplication.translate("Dialog", "Add from file...", None, QtGui.QApplication.UnicodeUTF8))


if __name__ == "__main__":
    import sys
    app = QtGui.QApplication(sys.argv)
    Dialog = QtGui.QDialog()
    ui = Ui_Dialog()
    ui.setupUi(Dialog)
    Dialog.show()
    sys.exit(app.exec_())

