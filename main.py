#!/usr/bin/env python2
# coding: utf-8
#                        VENI, SANCTE SPIRITUS

import logging
from os.path import expanduser, join
from src import main
from PySide import QtGui, QtCore
import sys
from src import pyside_excepthook

app = QtGui.QApplication(sys.argv)
app.setApplicationName("Visual Web Scraper")

projectname = main.Ui()
projectname.show()

# check that local directories exists
homedir = QtCore.QDir.home()

if not homedir.exists("Visual Web Scraper"):
    homedir.mkdir("Visual Web Scraper")

# check that the output dir exists
if not homedir.exists("Visual Web Scraper/Scrap output"):
    homedir.mkdir("Visual Web Scraper/Scrap output")

if not homedir.exists("Visual Web Scraper/Selections"):
    homedir.mkdir("Visual Web Scraper/Selections")

if not homedir.exists("Visual Web Scraper/Debug"):
    homedir.mkdir("Visual Web Scraper/Debug")

# Setup logging here
logging.basicConfig(level=logging.DEBUG,
                    format=("[%(levelname)-8s] %(asctime)s %(name)-12s"
                            " %(funcName)-21s - L%(lineno)-04d: %(message)s"),
                    datefmt="%m-%d %H:%M.%S",
                    filename=join(
                        expanduser("~"), "Visual Web Scraper",
                        "Debug", "scraper.log"),
                    filemode="w")

sys.excepthook = pyside_excepthook.excepthook

app.exec_()
