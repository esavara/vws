# This file is not part of the project Visual web scraper
#
# Updates the python files of Qt's user interface definition files (.ui) and
# Qt's resources files (*.qrc)

for arch in ui/*.ui;
do
    [ "$arch" = 'ui/*.ui' ] && [ ! -e 'ui/*.ui' ] && echo 'No .ui files found!' && break
    echo "Updating python file for '$arch'";
    python2-pyside-uic --from-imports -x -o "src/ui/`basename ${arch/.ui/_ui.py}`" "$arch";
done;

for arch in res/*.qrc;
do
    [ "$arch" = 'res/*.qrc' ] && [ ! -e 'res/*.qrc' ] && echo 'No .qrc files found!' && break
    echo "Updating python files for '$arch'";
    pyside-rcc -o "src/ui/`basename ${arch/.qrc/_rc.py}`" "$arch";
done;
