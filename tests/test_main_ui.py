#!/usr/bin/env python2
# coding: utf-8
#                        VENI, SANCTE SPIRITUS

from nose.tools import eq_, timed
from PySide.QtCore import Qt
from PySide.QtTest import QTest
from src import main

ui = main.Ui()


@timed(10)
def test_default_notabs():
    eq_(ui.tbwTab.count(), 0)


@timed(10)
def test_createnewtab():
    QTest.mouseClick(ui.tbtCorner, Qt.LeftButton)
    eq_(ui.tbwTab.count(), 1)


@timed(10)
def test_switchtowebscraper():
    QTest.mouseClick(ui.tbwTab.widget(0).menu.pbtWebsite,
                     Qt.LeftButton)
    eq_(ui.tbwTab.widget(0).stacked.currentIndex(), 1)
    # Cleaning...
    ui.tbwTab.tabCloseRequested.emit(0)
    QTest.mouseClick(ui.tbtCorner, Qt.LeftButton)


@timed(10)
def test_switchtoapi():
    QTest.mouseClick(ui.tbwTab.widget(0).menu.pbtAPI,
                     Qt.LeftButton)
    eq_(ui.tbwTab.widget(0).stacked.currentIndex(), 2)
    # Cleaning...
    ui.tbwTab.tabCloseRequested.emit(0)
