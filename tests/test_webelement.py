# !/usr/bin/env python2
# coding: utf-8
#                        VENI, SANCTE SPIRITUS

from os.path import dirname

import nose
from nose.tools import timed
from PySide import QtGui, QtWebKit, QtCore
from src import webelementinfo

testdir = QtCore.QDir(dirname(__file__))
fileuri = QtCore.QUrl(
    "file://" + testdir.absoluteFilePath("html/simple.html"))


@timed(10)
def test_create_webelement():
    """ Simply create a WebElement instance
    """
    # main frame to set the HTML content
    webview = QtWebKit.QWebView()
    # event loop helps to wait until the page is loaded
    waitloaded = QtCore.QEventLoop()
    webview.loadFinished.connect(waitloaded.exit)
    # load the file as a URL
    webview.load(fileuri)
    # wait until it is loaded
    waitloaded.exec_()

    mainframe = webview.page().mainFrame()
    qwebel = mainframe.findFirstElement("html body article div#comments")

    webel = webelementinfo.WebElement(qwebel, 1)
    nose.tools.ok_(not webel.isNull(), "WebElement instance is Null")


@timed(10)
def test_make_webelement_child():
    """ Create two WebElements, one will be the child of the other
    """
    # main frame to set the HTML content
    webview = QtWebKit.QWebView()
    # event loop helps to wait until the page is loaded
    waitloaded = QtCore.QEventLoop()
    webview.loadFinished.connect(waitloaded.exit)
    # load the file as a URL
    webview.load(fileuri)
    # wait until it is loaded
    waitloaded.exec_()

    mainframe = webview.page().mainFrame()
    qwebelpar = mainframe.findFirstElement("html body article div#comments")
    qwebelchi = mainframe.findFirstElement(
        "html body article div#comments > div.comment")
    webelpar = webelementinfo.WebElement(qwebelpar, 1)
    webelchi = webelementinfo.WebElement(qwebelchi, 2)
    webelpar.setchild("child", webelchi)
    nose.tools.ok_(webelchi._parent, "The child WebElement is orphan!")


@timed(10)
def test_get_webelement_child():
    """ Ask the parent to return its child by name
    """
    # main frame to set the HTML content
    webview = QtWebKit.QWebView()
    # event loop helps to wait until the page is loaded
    waitloaded = QtCore.QEventLoop()
    webview.loadFinished.connect(waitloaded.exit)
    # load the file as a URL
    webview.load(fileuri)
    # wait until it is loaded
    waitloaded.exec_()

    mainframe = webview.page().mainFrame()
    qwebelpar = mainframe.findFirstElement("html body article div#comments")
    qwebelchi = mainframe.findFirstElement(
        "html body article div#comments > div.comment")
    webelpar = webelementinfo.WebElement(qwebelpar, 1)
    webelchi = webelementinfo.WebElement(qwebelchi, 2)
    webelpar.setchild("child", webelchi)
    r = webelpar.getchild("child")
    nose.tools.eq_(r, webelchi, "Returned WebElement is not the same object")


@timed(10)
def test_get_webelement_csspath():
    """ Get absolute CSS selector path correctly
    """
    # main frame to set the HTML content
    webview = QtWebKit.QWebView()
    # event loop helps to wait until the page is loaded
    waitloaded = QtCore.QEventLoop()
    webview.loadFinished.connect(waitloaded.exit)
    # load the file as a URL
    webview.load(fileuri)
    # wait until it is loaded
    waitloaded.exec_()

    mainframe = webview.page().mainFrame()
    qwebel = mainframe.findFirstElement("html body article div#comments")
    webel1 = webelementinfo.WebElement(qwebel, 1)
    webel2 = webelementinfo.WebElement(qwebel, 2)
    r2 = webel2.getcsspath()
    nose.tools.eq_(r2, "html body article > h1 + div#comments")
    r1 = webel1.getcsspath()
    nose.tools.eq_(r1, "html body article > div#comments")


@timed(10)
def test_getasdict_webelement():
    """ Get the dict representation of a WebElement object
    """
    # main frame to set the HTML content
    webview = QtWebKit.QWebView()
    # event loop helps to wait until the page is loaded
    waitloaded = QtCore.QEventLoop()
    webview.loadFinished.connect(waitloaded.exit)
    # load the file as a URL
    webview.load(fileuri)
    # wait until it is loaded
    waitloaded.exec_()

    mainframe = webview.page().mainFrame()
    qwebel = mainframe.findFirstElement("html body article div#comments")
    webel = webelementinfo.WebElement(qwebel, 1)
    r = webel.getasdict()
    nose.tools.eq_(
        r,
        {"csspath": "<main>:1:html body article > div#comments"})


@timed(10)
def test_getasdict_withchild_webelement():
    """ Get the dict representation of a WebElement object, child included
    """
    # main frame to set the HTML content
    webview = QtWebKit.QWebView()
    # event loop helps to wait until the page is loaded
    waitloaded = QtCore.QEventLoop()
    webview.loadFinished.connect(waitloaded.exit)
    # load the file as a URL
    webview.load(fileuri)
    # wait until it is loaded
    waitloaded.exec_()

    mainframe = webview.page().mainFrame()
    qwebelpar = mainframe.findFirstElement(
        "html body article > div#comments")
    qwebelchi = mainframe.findFirstElement(
        "html body article div#comments > div.comment")
    webelpar = webelementinfo.WebElement(qwebelpar, 1)
    webelchi = webelementinfo.WebElement(qwebelchi, 2)
    webelpar.setchild("child", webelchi)

    r = webelpar.getasdict()

    nose.tools.eq_(
        r,
        {'children':
         {'child':
          {'csspath':
           '<main>:2:html body article div#comments > h2 + div.comment'}},
         'csspath': '<main>:1:html body article > div#comments'})


@timed(10)
def test_webelement_setchildren():
    """ From a major WebElement, set all the minor elements that
    we can find
    """
    # main frame to set the HTML content
    webview = QtWebKit.QWebView()
    # event loop helps to wait until the page is loaded
    waitloaded = QtCore.QEventLoop()
    webview.loadFinished.connect(waitloaded.exit)
    # load the file as a URL
    webview.load(fileuri)
    # wait until it is loaded
    waitloaded.exec_()

    mainframe = webview.page().mainFrame()
    qwebelpar = mainframe.findFirstElement(
        "html body article > div#comments")
    qwebelchi = mainframe.findFirstElement(
        "html body article div#comments > div.comment")
    webelpar = webelementinfo.WebElement(qwebelpar, 1)
    webelchi = webelementinfo.WebElement(qwebelchi, 2)

    # set the rest of the children
    webelpar.setchildren("child", webelchi, webelchi.getcsspath(), 2)

    # get one of the children
    webelany = webelpar.getchild("child_0")
    nose.tools.ok_(webelany)


@timed(10)
def test_webelement_setchildtochildren():
    """ For each child of a major WebElement set a child by CSS path
    """
    # main frame to set the HTML content
    webview = QtWebKit.QWebView()
    # event loop helps to wait until the page is loaded
    waitloaded = QtCore.QEventLoop()
    webview.loadFinished.connect(waitloaded.exit)
    # load the file as a URL
    webview.load(fileuri)
    # wait until it is loaded
    waitloaded.exec_()

    mainframe = webview.page().mainFrame()
    qwebelpar = mainframe.findFirstElement(
        "html body article > div#comments")
    qwebelchi = mainframe.findFirstElement(
        "html body article div#comments > div.comment")
    qwebelchichi = mainframe.findFirstElement(
        "html body article div#comments div.comment > div.author")
    webelpar = webelementinfo.WebElement(qwebelpar, 0)
    webelchi = webelementinfo.WebElement(qwebelchi, 1)
    webelchichi = webelementinfo.WebElement(qwebelchichi, 2)

    # set the rest of the children
    webelpar.setchildren("child", webelchi, webelchi.getcsspath(), 1)

    # now set a child for every child of webelpar
    webelpar.setchildtochildren("subchild", webelchichi.getcsspath(), 2)

    # finally, get a child of webelpar and then get the child from that child
    rr = webelpar.getchild("child_2")
    nose.tools.ok_(rr)
    r = rr.getchild("subchild")
    nose.tools.ok_(r)


@timed(10)
def test_webelement_nonzero():
    """ Test the truthness of the object
    """
    # main frame to set the HTML content
    webview = QtWebKit.QWebView()
    # event loop helps to wait until the page is loaded
    waitloaded = QtCore.QEventLoop()
    webview.loadFinished.connect(waitloaded.exit)
    # load the file as a URL
    webview.load(fileuri)
    # wait until it is loaded
    waitloaded.exec_()

    mainframe = webview.page().mainFrame()
    # This element do not exists
    qwebelpar = mainframe.findFirstElement(
        "html body thistagnotexists")
    webelpar = webelementinfo.WebElement(qwebelpar, 1)
    nose.tools.ok_(not webelpar)
