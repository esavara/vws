# Visual Web Scraper: VWS [![pipeline status](https://gitlab.com/esavara/vws/badges/master/pipeline.svg)](https://gitlab.com/esavara/vws/commits/master) [![coverage report](https://gitlab.com/esavara/vws/badges/master/coverage.svg)](https://gitlab.com/esavara/vws/commits/master)

Point and click what comments from websites you want to scrap 🔪
