In this directory we implement the API Scraper server and its plugin plugins. This is basically a webapp started by the main project written in Python. The main webapp lists a series of sub web apps that can be use to request data from different services as, i.e.: Twitter Stream API, get its data and store it in CSV files.

The server directory contains the source code of the main web app which is started by the main project written in Python. Other directories implement an individual set of sub web apps that can be started from the main web app on-demand.

For this epic stage of this project development, we are going to use the libraries [Negroni](https://github.com/codegangsta/negroni) and [Gorilla/mux](http://www.gorillatoolkit.org/pkg/mux) with [Bootstrap 3](http://getbootstrap.com/) to make things easier and lightweight. 
