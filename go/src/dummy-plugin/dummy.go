package main

import (
	"../server/plugininfo"
	"flag"
	// "github.com/codegangsta/negroni"
	// "github.com/gorilla/mux"
	// "gopkg.in/unrolled/render.v1"
	// "net/http"
	// "path/filepath"
)

func main() {
	var info = flag.Bool("info", true, "output plugin information as a JSON string")
	flag.Parse()
	if *info == true {
		myinfo := plugininfo.Plugin{Human_name: "Dummy Plugin 1",
			Machine_name: "dummy1",
			Version:      "0.1",
			Execpath:     "/tmp/dumm1"}
		myinfo.ToJSON()
	}
}
