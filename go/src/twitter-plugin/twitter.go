package main

import (
	"../server/plugininfo"
	"./handlers"
	"encoding/json"
	"flag"
	"github.com/codegangsta/negroni"
	"github.com/darkhelmet/twitterstream"
	"github.com/gorilla/mux"
	"github.com/kardianos/osext"
	"gopkg.in/unrolled/render.v1"
	"log"
	"net"
	"net/http"
	"os"
	"os/user"
	"path/filepath"
	"strings"
)

type TwitterPluginConf struct {
	Consumer_key    string
	Consumer_secret string
	Access_token    string
	Access_secret   string
	Keywords        []string
}

func freeport() string {
	// get a free port assigned by the operating system
	listen, _ := net.Listen("tcp", ":0")
	defer listen.Close()
	addr := listen.Addr().String()
	// slice the string to get just the port
	rindex := strings.LastIndex(addr, ":")
	port := addr[rindex:]
	return "127.0.0.1" + port
}

func main() {
	usr, _ := user.Current()
	filename := filepath.Join(usr.HomeDir, "Visual Web Scraper", "Debug", "twitter.log")
	f, err := os.OpenFile(filename, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	if err == nil {
		log.SetOutput(f)
	}
	defer f.Close()
	log.Println("Started")
	// check if some directories exists, if not, create them
	_, err = os.Stat(filepath.Join(usr.HomeDir, "Visual Web Scraper", "Plugins", "conf"))
	if err != nil {
		os.MkdirAll(filepath.Join(usr.HomeDir, "Visual Web Scraper", "Plugins", "conf"), 0777)
	}
	_, err = os.Stat(filepath.Join(usr.HomeDir, "Visual Web Scraper", "Plugins", "output", "twitter"))
	if err != nil {
		os.MkdirAll(filepath.Join(usr.HomeDir, "Visual Web Scraper", "Plugins", "output", "twitter"), 0777)
	}
	var info = flag.Bool("info", false, "output plugin information as a JSON string")
	var run = flag.Bool("run", false, "start the web server and run the plugin")
	flag.Parse()
	if *info == true {
		// ask for the information of this plugin
		execpath, _ := osext.Executable()
		myinfo := plugininfo.Plugin{
			Human_name:   "Twitter Stream API",
			Machine_name: "twitter",
			Version:      "0.1",
			Execpath:     execpath}
		myinfo.ToJSON()
		log.Println("Information about myself returned")
	} else if *run == true {
		// start the web server
		pid := os.Getpid()
		execpath, _ := osext.ExecutableFolder()
		tmpldir := filepath.Join(execpath, "templates-twitter")
		r := render.New(render.Options{
			IsDevelopment: true,
			IndentJSON:    true,
			Extensions:    []string{".gtl", ".gohtml"},
			Layout:        "base",
			Directory:     tmpldir,
		})

		h := handler.Handler{Renderer: r, Client: nil, Connection: nil, Channel: make(chan int)}
		// load the configuration if exists
		conf := TwitterPluginConf{}
		file, err := os.Open(filepath.Join(execpath, "twitter-plugin.conf.json"))
		if err == nil {
			dec := json.NewDecoder(file)
			err := dec.Decode(&conf)
			if err != nil {
				log.Fatal(err)
			}
			h.Consumer_key = conf.Consumer_key
			h.Consumer_secret = conf.Consumer_secret
			h.Access_token = conf.Access_token
			h.Access_secret = conf.Access_secret
			h.Keywords = conf.Keywords
			h.Client = twitterstream.NewClient(h.Consumer_key, h.Consumer_secret, h.Access_token, h.Access_secret)
		}
		file.Close()

		m := mux.NewRouter()
		bootstrapdir := filepath.Join(filepath.Dir(filepath.Dir(execpath)), "bootstrap")
		m.PathPrefix("/static/").Handler(http.StripPrefix("/static/", http.FileServer(http.Dir(bootstrapdir))))

		m.HandleFunc("/counter", h.RenderCount) //.Headers("X-Requested-With", "XMLHttpRequest")
		m.HandleFunc("/", h.RenderIndex)
		m.HandleFunc("/auth", h.AuthPlugin).Methods("GET")
		m.HandleFunc("/auth", h.AuthPlugin).Methods("POST")
		m.HandleFunc("/stop", h.StopTwitter)

		n := negroni.Classic()
		n.UseHandler(m)
		addr := freeport()
		plgpid := plugininfo.PluginPid{
			Name: "twitter",
			Pid:  pid,
			Addr: addr}
		jsondata, _ := json.Marshal(plgpid)
		jsondata = append(jsondata, []byte("\n")...)
		os.Stdout.Write(jsondata)
		n.Run(addr)
	}
}
