package handler

import (
	"bitbucket.org/tebeka/strftime"
	"encoding/csv"
	"encoding/json"
	"github.com/darkhelmet/twitterstream"
	"gopkg.in/unrolled/render.v1"
	"log"
	"net/http"
	"os"
	"os/user"
	"path/filepath"
	"strconv"
	"strings"
	"time"
)

type Handler struct {
	Renderer        *render.Render
	Client          *twitterstream.Client
	Connection      *twitterstream.Connection
	Consumer_key    string
	Consumer_secret string
	Access_token    string
	Access_secret   string
	Keywords        []string
	Counter         int
	Channel         chan int
	Scraping        int
}

type TwitterPluginConf struct {
	Consumer_key    string
	Consumer_secret string
	Access_token    string
	Access_secret   string
	Keywords        []string
}

func (t *Handler) ReturnKeywords() string {
	return strings.Join(t.Keywords, ", ")
}

func (t *Handler) StartTwitter() (*twitterstream.Connection, error) {
	return t.Client.Track(t.Keywords...)
}

func (t *Handler) StopTwitter(w http.ResponseWriter, req *http.Request) {
	// when called, send a message through a channel to stop the goroutine
	// that runs the Twitter scraper
	t.Channel <- 1
}

func (t *Handler) RenderIndex(w http.ResponseWriter, req *http.Request) {
	// FIXME: load credential information here if none are set yet and create the
	// Twitterstream client too
	req.ParseForm()
	keywords := req.FormValue("keywords")
	// get since and until date range
	if keywords != "" {
		t.Keywords = strings.Split(keywords, ", ")
		conn, err := t.StartTwitter()
		if err != nil {
			log.Fatal(err)
		} else {
			t.Connection = conn
			// save the configuration here
			conf := TwitterPluginConf{t.Consumer_key, t.Consumer_secret, t.Access_token, t.Access_secret, t.Keywords}
			usr, _ := user.Current()
			file, err := os.Create(filepath.Join(usr.HomeDir, "Visual Web Scraper", "Plugins", "conf", "twitter-plugin.conf.json"))
			if err != nil {
				log.Fatal(err)
			}
			confjson, err := json.Marshal(conf)
			if err != nil {
				log.Fatal(err)
			}
			_, err = file.Write(confjson)
			if err != nil {
				log.Fatal(err)
			}
			file.Close()
			// anonymous(?) function that will get tweets
			go func() {
				// open a file for writing
				filename, _ := strftime.Format("output-%Y%m%d-%H%M%S.csv", time.Now())
				fileoutputdir := filepath.Join(usr.HomeDir, "Visual Web Scraper", "Plugins", "output", "twitter", filename)
				csvfile, _ := os.Create(fileoutputdir)
				// create a Writer object for writing the data
				// as csv
				writer := csv.NewWriter(csvfile)

				defer writer.Flush()
				defer csvfile.Close()

				t.Counter = 0
				t.Scraping = 1
				for {
					select {
					case <-t.Channel:
						log.Println("Stopping scraper")
						t.Scraping = 0
						return
					default:
						tweet, err := t.Connection.Next()
						if err == nil {
							writer.Write([]string{tweet.IdString, tweet.User.ScreenName, tweet.Lang, strconv.FormatFloat(tweet.Coordinates.Lat.Float64(), 'f', 6, 64), strconv.FormatFloat(tweet.Coordinates.Long.Float64(), 'f', 6, 64), tweet.Text})
							t.Counter += 1
						} else {
							log.Println(err)
							t.Scraping = 0
							return
						}
					}
				}

			}()
			// end of the anonymous function
		}
	}
	t.Renderer.HTML(w, http.StatusOK, "index", t)
}

func (t *Handler) RenderCount(w http.ResponseWriter, req *http.Request) {
	t.Renderer.JSON(w, http.StatusOK, map[string]int{"count": t.Counter, "scraping": t.Scraping})
}

func (t *Handler) CheckCredentialsSet() bool {
	// return true if all credentials are not set otherwise return false
	if t.Consumer_key == "" || t.Consumer_secret == "" || t.Access_token == "" || t.Access_secret == "" {
		return true
	} else {
		return false
	}
}

func (t *Handler) AuthPlugin(w http.ResponseWriter, req *http.Request) {
	// called to render the authentication form for the plugin
	if req.Method == "GET" {
		t.Renderer.HTML(w, http.StatusOK, "auth", t)
	} else if req.Method == "POST" {
		// parse the form
		req.ParseForm()
		// input validation is handled by the form, however, maybe some
		// form validation is required here from server side.
		t.Consumer_key = req.FormValue("consumer_key")
		t.Consumer_secret = req.FormValue("consumer_secret")
		t.Access_token = req.FormValue("access_token")
		t.Access_secret = req.FormValue("access_secret")
		t.Client = twitterstream.NewClient(t.Consumer_key, t.Consumer_secret, t.Access_token, t.Access_secret)
		// The Twitter client is ready, we can go back to the index
		http.Redirect(w, req, "/", 302)
	}
}
