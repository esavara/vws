// This package is the backbone of API Scraper - this executable is started by
// the main part of the project (The Python part) showing a list of plugins,
// which are nothing but other executable applications that allow the user to
// scrap many different APIs.
//
// This application can be accessed through a web browser, the port is always a
// random one for each run, the main part of the project always know which port
// visit to access API Scraper's main UI.
//
// About plugins: How to make a compatible one?
//
// As said before, a plugin is just another executable Go application installed
// at `%HOMEUSER%\Visual Web Scraper\Plugins`. A plugin MUST have two flag called
// -info and -run. -info outputs as JSON the information about itself, things
// like:
//
//   - Human_name: the human readable name of the plugin
//   - Machine_name: the machine readable name of the plugin
//   - Version: The version of the plugin.
//   - Execpath: The full path of the executable file
//
// For the -run option, when given, it should start the internal web server
// after outputting as JSON things like:
//   - Name: The machine readable name of the plugin
//   - Pid: the pid of the application (that you can get with os.Getpid)
//   - Addr: The full IP address, which should look like http://127.0.0.1:1337,
//   here "1337" is a random port.
//
// This information is CRITICAL for the "backbone" of API Scraper. When a
// plugin is request to start by the user, API Scraper will start the execution
// of such application with the -run flag.
//
// For more information about the internals of a plugin, take a look at the
// plugin "Twitter Stream API" source code.
package main
