package plugininfo

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"log"
	"os"
	"os/exec"
	"os/user"
	"path/filepath"
)

type PluginPid struct {
	Name string
	Pid  int
	Addr string
}

type Plugin struct {
	Human_name   string
	Machine_name string
	Version      string
	Execpath     string
}

func (p *Plugin) ToJSON() {
	b, err := json.Marshal(p)
	if err != nil {
		log.Fatal(err)
	}
	os.Stdout.Write(b)
}

func ListPlugins() []Plugin {
	currentuser, _ := user.Current()
	pluginspath := filepath.Join(currentuser.HomeDir, "Visual Web Scraper", "Plugins", "plugins")
	files, _ := ioutil.ReadDir(pluginspath)
	plugins := []Plugin{}
	for _, f := range files {
		log.Println("Checking", f.Name(), "which have mode", f.Mode(), ", but is a directory?", f.IsDir())
		if f.IsDir() == false {
			log.Println(f.Name(), "is a executable file (at least it is not a directory)")
			pluginexec, _ := filepath.Abs(filepath.Join(pluginspath, f.Name()))
			var output bytes.Buffer
			cmdplugin := exec.Command(pluginexec, "-info")
			cmdplugin.Stdout = &output
			err := cmdplugin.Run()
			if err != nil {
				log.Fatal(err)
			}
			// parse the JSON data here
			var plugininfo Plugin
			err = json.Unmarshal(output.Bytes(), &plugininfo)
			if err != nil {
				log.Fatal(err)
			}
			plugins = append(plugins, plugininfo)
			log.Println("Information gathered for plugin", plugininfo.Human_name, "located at", plugininfo.Execpath)
		} else {
			log.Println(f.Name(), "is a executable file, ignoring!")
		}
	}
	return plugins
}

func MapPlugins() map[string]string {
	plugins := ListPlugins()
	plgmap := make(map[string]string)
	for _, plg := range plugins {
		plgmap[plg.Machine_name] = plg.Execpath
	}
	return plgmap
}
