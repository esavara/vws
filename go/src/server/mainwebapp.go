package main

import (
	"./handlers"
	"github.com/codegangsta/negroni"
	"github.com/gorilla/mux"
	"github.com/kardianos/osext"
	"gopkg.in/unrolled/render.v1"
	"log"
	"net"
	"net/http"
	"os"
	"os/user"
	"path/filepath"
)

// get a free port assigned by the operating system
func freeport() string {
	listen, _ := net.Listen("tcp", ":0")
	defer listen.Close()
	return listen.Addr().String()
}

func main() {
	usr, _ := user.Current()
	filename := filepath.Join(usr.HomeDir, "Visual Web Scraper", "Debug", "mainwebapp.log")
	f, err := os.OpenFile(filename, os.O_RDWR|os.O_CREATE, 0666)
	if err == nil {
		log.SetOutput(f)
	}
	defer f.Close()
	execdir, _ := osext.ExecutableFolder()
	tmpldir := filepath.Join(execdir, "templates")
	r := render.New(render.Options{
		IsDevelopment: true,
		IndentJSON:    true,
		Extensions:    []string{".gtl", ".gohtml"},
		Layout:        "base",
		Directory:     tmpldir})

	pidlist := make(map[int]string, 10)
	t := handler.Handler{Renderer: r, Plgpid: pidlist}

	m := mux.NewRouter()
	// route to the static data
	bootstrapdir := filepath.Dir(filepath.Dir(execdir))
	bootstrapdir = filepath.Join(bootstrapdir, "bootstrap")
	m.PathPrefix("/static/").Handler(http.StripPrefix("/static/", http.FileServer(http.Dir(bootstrapdir))))

	// this may be better?
	m.HandleFunc("/", t.RenderIndex)
	m.HandleFunc("/open/{pluginname}", t.LaunchPlugin)

	n := negroni.Classic()
	n.UseHandler(m)
	addr := freeport()
	os.Stdout.Write([]byte(addr + "\n"))
	log.Println("Started!")
	log.Println("Listening on:", addr)
	n.Run(addr)
}
