package handler

import (
	"../plugininfo"
	"bytes"
	"encoding/json"
	"github.com/gorilla/mux"
	"gopkg.in/unrolled/render.v1"
	"log"
	"net/http"
	"os/exec"
	"strings"
	"time"
)

type Handler struct {
	Renderer *render.Render
	Plgpid   map[int]string
}

func (t *Handler) RenderIndex(w http.ResponseWriter, req *http.Request) {
	plugins := plugininfo.ListPlugins()
	t.Renderer.HTML(w, http.StatusOK, "index", plugins)
}

func (t *Handler) LaunchPlugin(w http.ResponseWriter, req *http.Request) {
	plugins := plugininfo.MapPlugins()
	vars := mux.Vars(req)
	pluginname := vars["pluginname"]
	exepath := plugins[pluginname]
	log.Println("Launching plugin", pluginname, "located at", exepath)

	// launching the executable
	var output bytes.Buffer
	cmdp := exec.Command(exepath, "-run")
	cmdp.Stdout = &output
	go cmdp.Run()
	// wait a little
	time.Sleep(2 * time.Second)
	var pluginpid plugininfo.PluginPid
	outputstr := strings.Split(output.String(), "\n")[0]
	if outputstr != "" {
		err := json.Unmarshal(bytes.NewBufferString(outputstr).Bytes(), &pluginpid)
		if err != nil {
			// render a json string here and send it
			log.Fatal(err)
		} else {
			// redirect the user to the plugin
			t.Plgpid[pluginpid.Pid] = pluginpid.Name
			http.Redirect(w, req, "http://"+pluginpid.Addr, 302)
		}
	} else {
		// there was an error, the application didn't output something
		http.Redirect(w, req, "/", 302)
	}
}
