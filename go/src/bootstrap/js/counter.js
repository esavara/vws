//initializing odometer
window.odometerOptions = {
    duration: 1000
};
// implements AJAX feature for updating the counter of different plugins
$(document).ready(function() {
    try {
        if (!credentialsNeeded) {
            var counterInterval = undefined;
            $.get("/counter", function(data) {
                if (data.scraping == 0)
                    $("#submit").removeClass("disabled").removeAttr(
                        "disabled");
                else {
                    $("#counter").text(data.count);
                    $("#stop").addClass("in");
                    $("#submit").addClass("disabled").attr("disabled",
                        "disabled").text("Started");
                    counterInterval = setInterval(function() {
                        $.get("/counter", function(data) {
                            if (data.scraping != 0) {
                                $("#stop").removeClass(
                                    "disabled");
                                $("#counter").text(data.count);
                            }
                        });
                    }, 2500);
                }
            });
            $("#keywords-scrap-form").submit(function(e) {
                e.preventDefault();
                $("#submit").addClass("disabled").attr("disabled",
                    "disabled")
                    .text("Please wait...");
                $.post("/", {
                    "keywords": $("#keywords").val(),
                    "submit": "yes"
                }, function(data) {
                    $("#submit").text("Started");
                    $("#stop").addClass("in");
                    counterInterval = setInterval(function() {
                        $.get("/counter", function(data) {
                            if (data.scraping != 0) {
                                $("#stop").removeClass(
                                    "disabled");
                                $("#counter").text(data.count)
                            }
                        });
                    }, 2500);
                });
            });
            $("#stop").click(function() {
                clearInterval(counterInterval);
                $(this).text("Stopping...");
                $.get("/stop", function() {
                    $("#stop").removeClass("in");
                    $("#submit").removeClass("disabled").removeAttr(
                        "disabled").text("Start!");
                });
            });
        }
    } catch (e) {}
});